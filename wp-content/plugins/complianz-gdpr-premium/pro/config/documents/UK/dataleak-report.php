<?php
defined('ABSPATH') or die("you do not have acces to this page!");
$this->pages['uk']['dataleak']['document_elements'] = array(
    array(
        'content' => sprintf('Date: %s', '[publish_date]'),
    ),
    array(
        'content' => 'RE: Information regarding personal data breaches',
    ),
    array(
        'content' => 'Dear Sir/Madam,',
    ),
    array(
        'content' => 'With this letter, I would like to inform you of a recently discovered security incident in our organisation.',
    ),
    array(
        'content' => 'In that incident, personal data was lost and there is no current back-up copy of that personal data.',
        'condition' => array(
            'type-of-dataloss-uk' => 1,
        )
    ),
    array(
        'content' => 'As a result of that incident, we cannot rule out the possibility that unauthorised persons have had access to your personal data. ',
        'condition' => array(
            'type-of-dataloss-uk' => 2,
        )
    ),
    array(
        'content' => 'We have therefore notified the national supervisory authority. As we expect possible adverse consequences for your privacy, we also inform you as a data subject. We would like to provide you with the following information in order to limit the possible consequences for you:',
        'callback_condition' => 'cmplz_dataleak_has_to_be_reported',

    ),
    array(
        'title' => 'Explanation of the nature of the breach:',
        'content' => '[what-occurred-uk]',
        'condition' => array('risk-of-data-loss-uk'=>3),
    ),
    array(
        'title' => 'Possible consequences:',
        'content' => '[consequences-uk]',
        'condition' => array('risk-of-data-loss-uk'=>3),
    ),
    array(
        'title' => 'Measures we have taken:',
        'content' => '[measures-uk]',
        'condition' => array('risk-of-data-loss-uk'=>3),
    ),
    array(
        'title' => 'Measures a person involved can take to minimise damage:',
        'content' => '[measures_by_person_involved-uk]',
        'condition' => array('risk-of-data-loss-uk'=>3),
    ),
    array(
        'content' => 'Despite these measures we have taken, the security breach may have adverse consequences for your privacy. To limit these as much as possible, we recommend that you take a number of measures. We hope that this letter has provided you with sufficient information about the security incident and its consequences. We are continuously working to improve security and counteract the possible consequences of this breach. We would like to apologise for any inconvenience you have experienced to date. ',
    ),
    array(
        'content' => sprintf('If you would like more information about the data breach, please send a message to %s', '[email_company]'),
    ),
    array(
        'content' => 'Kind regards, ',
    ),
    array(
        'content' => '[organisation_name]<br>
                    [address_company]<br>
                    [country_company]<br>
                    ' . 'Website:' . ' [domain] <br>
                    ' . 'Email:' . ' [email_company] <br>
                    [telephone_company]',
    ),
);
