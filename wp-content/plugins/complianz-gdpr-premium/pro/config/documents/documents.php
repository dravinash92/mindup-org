<?php
defined('ABSPATH') or die("you do not have access to this page!");
$this->pages['eu']['privacy-statement'] = array(
        'title' => __("Privacy Statement (EU)", 'complianz-gdpr'),
        'document_elements' => '',
        'condition' => array(
            'privacy-statement' => 'generated',
            'regions' => 'eu',
        ),
        'public' => true,

);

$this->pages['eu']['impressum'] = array(
	'title' => __("Impressum", 'complianz-gdpr'),
	'document_elements' => '',
	'condition' => array(
		'impressum' => 'generated',
		'regions' => 'eu',
		'eu_consent_regions' => 'yes',
	),
	'public' => true,
);

$this->pages['us']['privacy-statement'] = array(
        'title' => __("Privacy Statement (US)", 'complianz-gdpr'),
        'condition' => array(
            'privacy-statement' => 'generated',
            'regions' => 'us',
        ),
        'public' => true,

);
$this->pages['us']['privacy-statement-children'] =
	array(
        'title' => __("Privacy Statement for Children (US)", 'complianz-gdpr'),
        'document_elements' => '',
        'condition' => array(
            'privacy-statement' => 'generated',
            'targets-children' => 'yes',
            'regions' => 'us',
        ),
        'public' => true,

);
$this->pages['uk']['privacy-statement'] =
	array(
        'title' => __("Privacy Statement (UK)", 'complianz-gdpr'),
        'document_elements' => '',
        'condition' => array(
            'privacy-statement' => 'generated',
            'regions' => 'uk',
        ),
        'public' => true,

);

$this->pages['uk']['privacy-statement-children'] =
	array(
        'title' => __("Privacy Statement for Children (UK)", 'complianz-gdpr'),
        'document_elements' => '',
        'condition' => array(
            'privacy-statement' => 'generated',
            'targets-children' => 'yes',
            'regions' => 'uk',
        ),
        'public' => true,

);

$this->pages['ca']['privacy-statement'] =  array(
	    'title' => __("Privacy Statement (CA)", 'complianz-gdpr'),
	    'condition' => array(
		    'privacy-statement' => 'generated',
		    'regions' => 'ca',
	    ),
	    'public' => true,
);

$this->pages['ca']['privacy-statement-children'] = array(
	    'title' => __("Privacy Statement for Children (CA)", 'complianz-gdpr'),
	    'document_elements' => '',
	    'condition' => array(
		    'privacy-statement' => 'generated',
		    'targets-children' => 'yes',
		    'regions' => 'ca',
	    ),
	    'public' => true,

);
$this->pages['all']['disclaimer'] =
	array(
        'title' => __("Disclaimer", 'complianz-gdpr'),
        'document_elements' => '',
        'condition' => array('disclaimer' => 'generated'),
        'public' => true,

);

$this->pages['eu']['processing'] = array(
        'title' => _x('Processing Agreement', 'Title on processing agreement page', 'complianz-gdpr'),
        'public' => false,
        'condition' => array(
	        'regions' => 'eu',
        ),
    );
$this->pages['us']['processing'] = array(
        'title' => _x('Processing Agreement', 'Title on processing agreement page', 'complianz-gdpr'),
        'public' => false,
        'condition' => array(
            'regions' => 'us',
        ),
    );
$this->pages['uk']['processing'] =  array(
        'title' => _x('Processing Agreement', 'Title on processing agreement page', 'complianz-gdpr'),
        'public' => false,
        'condition' => array(
            'regions' => 'uk',
        ),
    );
$this->pages['ca']['processing'] =  array(
	    'title' => _x('Processing Agreement', 'Title on processing agreement page', 'complianz-gdpr'),
	    'public' => false,
	    'condition' => array(
		    'regions' => 'ca',
	    ),
    );

$this->pages['eu']['dataleak'] =  array(
        'title' => __("Dataleak", 'complianz-gdpr'),
        'public' => false,
        'condition' => array(
	        'regions' => array('eu'),
        ),
    );
$this->pages['us']['dataleak'] =array(
        'title' => __("Dataleak", 'complianz-gdpr'),
        'public' => false,
        'condition' => array(
            'regions' => 'us',
        ),
    );
$this->pages['uk']['dataleak'] = array(
        'title' => __("Dataleak", 'complianz-gdpr'),
        'public' => false,
        'condition' => array(
            'regions' => 'uk',
        ),
    );

$this->pages['ca']['dataleak'] = array(
	    'title' => __("Dataleak", 'complianz-gdpr'),
	    'public' => false,
	    'condition' => array(
		    'regions' => 'ca',
	    ),
    );
