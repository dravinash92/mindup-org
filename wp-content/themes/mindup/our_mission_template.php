<?php
/**
 * Template Name: Our Mission Page Template
 *
 * Displays the Our Mission Page
 */
get_header(); ?>

<!-- Header : BEGIN -->
<header class="container-fluid padded-bottom-80 header-pages">
				
</header>
<!-- Header : END -->
<!-- Main Page : BEGIN -->
    <main class="container-fluid">
        <article class="row">

            <section class="col-12 padded-top-65 header-video h-100">
                <div class="container-lg">
                    <div class="row h-100">
                        <div class="col-12 h-100">
                            <div class="embed-responsive embed-responsive-16by9 h-100">
                                <video class="border_radius embed-responsive-item h-100" preload="none" poster="<?php the_field('banner_video_thumbnail'); ?>" autoplay loop>
                                    <source src="<?php the_field('banner_video'); ?>" type="video/mp4">
                                </video>
                            </div>
                        </div>

                    </div>
                </div>
        </section>

            <section class="col-12 top-mint-wave bg-mint padded-top-40">
                <div class="container-md">
                    <div class="row align-content-center">
                        <div class="col-12">
                            <h1 class="grad-border left"><?php the_field('mission_title'); ?></h1>
                        </div>
                        <div class="col-12 col-md-6 d-flex flex-column align-self-center ">
                            <h5><?php the_field('mission_heading'); ?></h5>
                            <p  class=""><?php the_field('mission_description'); ?></p>
                            
                        </div>
                        <!-- brain asset  -->
                        <div class="col-12 col-md-6 d-flex flex-column justify-content-center align-items-center">
                            <img class="img-fluid mission-img" src="<?php the_field('mission_image'); ?>">
                        </div>
                    </div>

                    <div class="row mission-blocks pt-5">
                        <div class="col-12 col-md-6 padded-bottom-40">
                            <div class="mission-block bg-blue">
                                <h6 class="sans-bold text-white"><?php the_field('card_heading_1'); ?></h6>
                                <ul class="mission-block-list">
                                    <?php the_field('card_description_1'); ?>
                                </ul>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 padded-bottom-40">
                            <div class="mission-block bg-blue">
                                <h6 class="sans-bold text-white"><?php the_field('card_heading_2'); ?></h6>
                                <ul class="mission-block-list">
                                <?php the_field('card_description_2'); ?>
                                </ul>
                            </div>
                        </div>
                        
                    </div>

                </div>
            </section>

            <section class="col-12 bg-grad-50-mint padded-bottom-40">
				<div class="container-md">
					<div class="row mission-blocks">
						<div class="col-12  pb-4">
							<div class="mission-block bg-blue">
								<div class="row ">
									<div class="col-12 col-md-6">
										<h6 class="sans-bold text-white"><?php the_field('card_heading_3'); ?></h6>
									</div>
									<div class="col-12 col-md-6">
										<ul class="mission-block-list">
										<?php the_field('card_description_3'); ?>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

            <section id="founder-message" class="col-12 padded-80">
                <div class="container-md">
                    <div class="row">
                        <div class="col-12 col-md-3 col-xl-3 pb-5 pb-md-0 d-flex flex-column justify-content-center align-items-center justify-content-md-start">
                            <img class="img-fluid goldie-headshot" src="<?php the_field('founder_image'); ?>"  />
                        </div>
                        <div class="col-12 col-md-9 col-xl-9">
							<h2 class="grad-border left padded-bottom-40"><?php the_field('founder_title'); ?></h2>

                                <?php the_field('founder_message'); ?>
                        </div>
                </div>	
            </section>

            <section id="board-directors" class="col-12 padded-80">
                <div class="container-md">
                    <div class="">
                        <div class="col-12">
                            <h2 class="grad-border left padded-bottom-40"><?php the_field('directors_title'); ?></h2>
                            <a href="#directorsVideoModal" role="button" class="video-header-container h-100" data-toggle="modal" data-target="#directorsVideoModal" title="MindUP animation FPO">
                                <div class="video-header-bar"></div>
                            </a>
                            <div class="modal fade " id="directorsVideoModal"  tabindex="-1"  aria-labelledby="videoModalLabel" aria-hidden="true" >
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span class="close-icon icon-remove-34x34 text-white "   aria-hidden="true"></span>
                            </button>
                            <div class="modal-dialog modal-xl modal-dialog-centered">                             
                                <div class="modal-content">
                                    <div class="modal-body video-body">
                                    <!-- popup video -->
                                        <div class="embed-responsive embed-responsive-16by9">
						                	<div class="embed-responsive-item">
											    <?php $upload_video = get_field('directors_video');?>
												<?php if($upload_video) { ?>
													<video id="modalVid" class="embed-responsive-item" crossorigin="Anonymous" preload="none" poster="<?php the_field('directors_video_thumbnail'); ?>" controls>
						  								<source src="<?php the_field('directors_video'); ?>" type="video/mp4">
						      						</video>
												<?php } 
												else { ?>
													<iframe class="embed-responsive-item" src="<?php the_field('directors_social_video'); ?>"></iframe>
												<?php } ?>
						      					<img src="<?php echo get_template_directory_uri(); ?>/lib/imgs/ui/icon/video-50x50.svg" class="play_icon" alt="">
						      				</div>
						         		</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <!-- Board Cards : BEGIN -->
                            <div class="row new-founder-columns row-cols-2 row-cols-sm-3 expert-cards row-cols-md-4 h-100 ">
                            <?php 
                                $directors_posts = get_field('directors_order');
                                
                                if( $directors_posts ): ?>
                                    
                                    <?php foreach( $directors_posts as $directors_post ): ?> 
                                        <div id="expert-cards-0" class="col card-col justify-content-center d-flex ">
                                            <div class="card h-100">
                                                
                                                                                    
                                                    <div style="background-image: url(<?php the_field('profile_image',$directors_post->ID); ?>);" class="card-img-top" aria-label=""></div>
                                                
                                                <div class="card-body">
                                                    <p class="card-text header"><?php the_field('profile_name',$directors_post->ID); ?></p>
                                                    <p class="card-text expert"><?php the_field('profile_designation',$directors_post->ID); ?></p>
                                                </div>								
                                                    
                                                <div class="card-footer">
                                                    <p class="text-center"><a href="<?php echo get_permalink($directors_post->ID);?>" target="_self" class="btn btn-primary btn-grad-1" title="title">Read Bio</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
							
                            <?php endif; ?>

                            </div>
                        <!-- Board Cards : END -->
                    </div>
                </div>
            </section>

            <section id="board-science" class="col-12 padded-80">
                <div class="container-md">
                    <div class="">
                        <div class="col-12">
                            <h2 class="grad-border left  padded-bottom-40"><?php the_field('advisor_title'); ?></h2>
                            <a href="#advisorsVideoModal" role="button" class="video-header-container h-100" data-toggle="modal" data-target="#advisorsVideoModal" title="MindUP animation FPO">
                                <div class="video-header-bar"></div>
                            </a>
                            <div class="modal fade " id="advisorsVideoModal"  tabindex="-1"  aria-labelledby="videoModalLabel" aria-hidden="true" >
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span class="close-icon icon-remove-34x34 text-white "   aria-hidden="true"></span>
                                </button>
                                <div class="modal-dialog modal-xl modal-dialog-centered">                             
                                    <div class="modal-content">
                                        <div class="modal-body video-body">
                                        <!-- popup video -->
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <div class="embed-responsive-item">
                                                    <?php $advisors_upload_video = get_field('advisors_video');?>
                                                    <?php if($advisors_upload_video) { ?>
                                                        <video id="modalVid" class="embed-responsive-item" crossorigin="Anonymous" preload="none" poster="<?php the_field('advisors_video_thumbnail'); ?>" controls>
                                                            <source src="<?php the_field('advisors_video'); ?>" type="video/mp4">
                                                        </video>
                                                    <?php } 
                                                    else { ?>
                                                        <iframe class="embed-responsive-item" src="<?php the_field('advisors_social_video'); ?>"></iframe>
                                                    <?php } ?>
                                                    <img src="<?php echo get_template_directory_uri(); ?>/lib/imgs/ui/icon/video-50x50.svg" class="play_icon" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Advisor Cards : BEGIN -->
                            <div class="row new-founder-columns row-cols-2 row-cols-sm-3 expert-cards row-cols-md-4 h-100 justify-content-between">
                            <?php 
                                $advisors_posts = get_field('advisors_order');
                                
                                if( $advisors_posts ): ?>
                                    
                                    <?php foreach( $advisors_posts as $advisors_post ): ?> 
                                        <div id="expert-cards-0" class="col card-col justify-content-center d-flex ">
                                            <div class="card h-100">
                                                
                                                                                    
                                                    <div style="background-image: url(<?php the_field('profile_image',$advisors_post->ID); ?>);" class="card-img-top" aria-label=""></div>
                                                
                                                <div class="card-body">
                                                    <p class="card-text header"><?php the_field('profile_name',$advisors_post->ID); ?></p>
                                                    <p class="card-text expert"><?php the_field('profile_designation',$advisors_post->ID); ?></p>
                                                </div>								
                                                    
                                                <div class="card-footer">
                                                    <p class="text-center"><a href="<?php echo get_permalink($advisors_post->ID);?>" target="_self" class="btn btn-primary btn-grad-1" title="title">Read Bio</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
							
                            <?php endif; ?>

                            </div>
                        <!-- Advisor Cards : END -->
                    </div>
                </div>
            </section>

            <section id="team" class="col-12 padded-80">
                <div class="container-md">
                    <div class="">
                        <div class="col-12">
                            <h2 class="grad-border left padded-bottom-40"><?php the_field('team_title'); ?></h2>
                            <a href="#teamVideoModal" role="button" class="video-header-container h-100" data-toggle="modal" data-target="#teamVideoModal" title="MindUP animation FPO">
                                <div class="video-header-bar"></div>
                            </a>
                            <div class="modal fade " id="teamVideoModal"  tabindex="-1"  aria-labelledby="videoModalLabel" aria-hidden="true" >
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span class="close-icon icon-remove-34x34 text-white "   aria-hidden="true"></span>
                                </button>
                                <div class="modal-dialog modal-xl modal-dialog-centered">                             
                                    <div class="modal-content">
                                        <div class="modal-body video-body">
                                        <!-- popup video -->
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <div class="embed-responsive-item">
                                                    <?php $team_upload_video = get_field('team_video');?>
                                                    <?php if($team_upload_video) { ?>
                                                        <video id="modalVid" class="embed-responsive-item" crossorigin="Anonymous" preload="none" poster="<?php the_field('team_video_thumbnail'); ?>" controls>
                                                            <source src="<?php the_field('team_video'); ?>" type="video/mp4">
                                                        </video>
                                                    <?php } 
                                                    else { ?>
                                                        <iframe class="embed-responsive-item" src="<?php the_field('team_social_video'); ?>"></iframe>
                                                    <?php } ?>
                                                    <img src="<?php echo get_template_directory_uri(); ?>/lib/imgs/ui/icon/video-50x50.svg" class="play_icon" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- team Cards : BEGIN -->
                            <div class="row new-founder-columns row-cols-2 row-cols-sm-3 expert-cards row-cols-md-4 h-100 ">
                            <?php 
                                $team_posts = get_field('team_order');
                                
                                if( $team_posts ): ?>
                                    
                                    <?php foreach( $team_posts as $team_post ): ?> 
                                        <div id="expert-cards-0" class="col card-col justify-content-center d-flex ">
                                            <div class="card h-100">
                                                
                                                                                    
                                                    <div style="background-image: url(<?php the_field('profile_image',$team_post->ID); ?>);" class="card-img-top" aria-label=""></div>
                                                
                                                <div class="card-body">
                                                    <p class="card-text header"><?php the_field('profile_name',$team_post->ID); ?></p>
                                                    <p class="card-text expert"><?php the_field('profile_designation',$team_post->ID); ?></p>
                                                </div>								
                                                    
                                                <div class="card-footer">
                                                    <p class="text-center"><a href="<?php echo get_permalink($team_post->ID);?>" target="_self" class="btn btn-primary btn-grad-1" title="title">Read Bio</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
							
                            <?php endif; ?>

                            </div>
                        <!-- team Cards : END -->
                    </div>
                </div>
            </section>

        </article>
    </main>
<!-- Main Page : END -->


<?php get_footer(); ?>
