<?php
/**
 * The header.
 *
 * This is the template that displays all of the <head> section and everything up until main.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage 
 * @since 1.0
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<?php wp_head(); ?>
	
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/lib/css/vendors/bootstrap/bootstrap.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/lib/css/styles.css">

	<!-- Favicon : BEGIN -->
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/lib/imgs/ui/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/lib/imgs/ui/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/lib/imgs/ui/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/lib/imgs/ui/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/lib/imgs/ui/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/lib/imgs/ui/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/lib/imgs/ui/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/lib/imgs/ui/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/lib/imgs/ui/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/lib/imgs/ui/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/lib/imgs/ui/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/lib/imgs/ui/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/lib/imgs/ui/favicon/favicon-16x16.png">
		<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/lib/imgs/ui/favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/lib/imgs/ui/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
	<!-- Favicon : END -->
	<style>
		.page-numbers li{
			display:inline;
			list-style:none;
		}
		</style>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
	<nav id="nav-home" class="navbar navbar-expand-xl navbar-light top-nav">
		<a href="">
			<div class="navbar-brand mindup-logo">
			</div>
		</a>
		<!-- Nav Guts : BEGIN -->
		<!-- CTA Buttons tablet : BEGIN -->
		<ul class="navbar-nav d-none d-md-flex d-xl-none flex-row ml-auto mx-xl-2 cta-buttons">
			<li class="nav-item"><a href="#" class="btn btn-grad-border mx-1 log-in"><div class="grad-inside white">Log In</div></a></li>
			<li class="nav-item"><a href="#" class="btn btn-secondary btn-grad-1 btn-small mx-1 get-started">Get Started</a></li>
		</ul>
		<!-- CTA Buttons tablet : END -->

		<!-- Menu Toggle : BEGIN -->
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
				<!-- <span class="icon-menu-46x46"></span> -->
			</button>
		<!-- Menu Toggle : END -->

		<!-- Menu contents : BEGIN -->
			<div class="navbar-collapse collapse" id="navbarScroll" style="justify-content: flex-end;">
				
				<!-- CTA Buttons mobile : BEGIN -->
					<ul class="navbar-nav d-flex d-md-none flex-row ml-auto mx-xl-2 cta-buttons">
						<li class="nav-item"><a href="#" class="btn btn-grad-border mx-1 log-in"><div class="grad-inside white">Log In</div></a></li>
						<li class="nav-item"><a href="#" class="btn btn-secondary btn-grad-1 btn-small mx-1 get-started">Get Started</a></li>
					</ul>
				<!-- CTA Buttons mobile : END -->
				
				<!-- Menu : BEGIN -->
					<?php wp_nav_menu(array('menu' => 'Main', 'sub_menu' => true, 'show_parent' => true, 'menu_class' => 'navbar-nav ml-auto mr-1', 'list_item_class'  => 'nav-item', 'link_class'   => 'dropdown-item')); ?>
					
				<!-- Menu : END -->
			</div>
		<!-- Menu contents : BEGIN -->

		<!-- CTA Buttons desktop : BEGIN -->
			<ul class="navbar-nav d-none d-xl-flex flex-row ml-auto mx-xl-2 cta-buttons">
				<li class="nav-item"><a href="#" class="btn btn-grad-border mx-1 log-in"><div class="grad-inside white">Log In</div></a></li>
				<li class="nav-item"><a href="#" class="btn btn-secondary btn-grad-1 btn-small mx-1 get-started">Get Started</a></li>
			</ul>
		<!-- CTA Buttons desktop : END -->
		<!-- Nav Guts : END -->

	</nav>