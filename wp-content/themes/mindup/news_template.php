<?php
/**
 * Template Name: News Page Template
 *
 * Displays the News Page
 */
get_header(); ?>

<!-- Header : BEGIN -->
	<header class="container-fluid padded-bottom-80 header-pages">
		<!--  -->	
	</header>
<!-- End : BEGIN -->

<!-- Main Page : BEGIN -->
	<main class="container-fluid">
		<article class="row">

			<section class="col-12 padded-80">
				<div class="container-md">
					
						<div class="col-12">
							<h1 class="grad-border left"><?php the_field('news_heading'); ?></h1>
						</div>
						<div class="row new-founder-columns row-cols-1 video-cards row-cols-md-3 h-100">
						<?php $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
						$pricing = array('post_type' => 'news_post','post_status'=>'publish','posts_per_page' => 10,'order' => 'ASC','meta_key'=>'sort','orderby'=>'meta_value_num','paged' => $paged,);
						$pricings = new WP_Query($pricing);
						if($pricings->have_posts()) : 
							while ($pricings->have_posts()) : $pricings->the_post(); $post_id = get_the_ID(); $news_date = get_the_date();?>
								<div id="video-cards-0" class="col card-col justify-content-center d-flex d-md-block">
									<div class="card h-100">
										<a href="#videoModal" role="button" class="video-header-container" data-toggle="modal" data-target="#news<?php echo $post_id ?>" title="MindUP animation FPO" >
																			
											<div style="background-image: url(<?php the_field('news_video_thumbnail'); ?>);" class="card-img-top" aria-label=""></div>
										</a>
										<div class="card-body">
											<p class="card-text date"><?php echo date("M Y", strtotime($news_date)); ?></p>
											<p class="card-text"><?php the_field('news_content'); ?></p>
										</div>								
											
										<div class="card-footer">
											<!-- Video Card link -->
											<p class="video-cta"><a href="#videoModal" role="button" class="video-header-container" data-toggle="modal" data-target="#news<?php echo $post_id ?>" title="MindUP animation FPO">Watch the Video</a></p>
										</div>
									</div>
								</div>
							<?php endwhile;?>
							
						<?php							
                        endif; wp_reset_query(); ?>
						
						</div>
						<?php
								$total_pages = $pricings->max_num_pages;

								if ($total_pages > 1){
							
									$current_page = max(1, get_query_var('paged'));
							
									$pagination = paginate_links(array(
										'base' => get_pagenum_link(1) . '%_%',
										'format' => '/page/%#%',
										'current' => $current_page,
										'total' => $total_pages,
										'prev_text'    => __('« Prev'),
										'next_text'    => __('Next »'),
										'type' => 'list'
									));
									echo "<nav class='text-center'>".$pagination."</nav>";
								}
							?>
						<?php $pricing = array('post_type' => 'news_post','post_status'=>'publish','order' => 'ASC','meta_key'=>'sort','orderby'=>'meta_value_num',);
						$pricings = new WP_Query($pricing);
						if($pricings->have_posts()) : while ($pricings->have_posts()) : $pricings->the_post(); $post_id = get_the_ID();?>
							<div class="modal fade " id="news<?php echo $post_id ?>" tabindex="-1"  aria-labelledby="videoModalLabel" aria-hidden="true" >
							     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							    	<span class="close-icon icon-remove-34x34 text-white "   aria-hidden="true"></span>
							    </button>
							  	<div class="modal-dialog modal-xl modal-dialog-centered">							          
							        <div class="modal-content">
							            <div class="modal-body video-body">
							            <!-- popup video -->
										<div class="embed-responsive embed-responsive-16by9">
						                	<div class="embed-responsive-item">
											<?php $upload_video = get_field('news_video');?>
												<?php if($upload_video) { ?>
													<video id="modalVid" class="embed-responsive-item" crossorigin="Anonymous" preload="auto" poster="<?php the_field('news_video_thumbnail'); ?>" controls>
						  								<source src="<?php the_field('news_video'); ?>" type="video/mp4">
						      						</video>
												<?php } 
												else { ?>
													<iframe class="embed-responsive-item" src="<?php the_field('news_social_video'); ?>"></iframe>
												<?php } ?>
						      					<img src="<?php echo get_template_directory_uri(); ?>/lib/imgs/ui/icon/video-50x50.svg" class="play_icon" alt="">
						      				</div>
						         		</div>
							            </div>
							        </div>
								</div>
							</div>
						<?php endwhile;
                        endif; wp_reset_query(); ?>
					
					
				</div>
			</section>
		</article>
	</main>
<!-- Main Page : END -->

<?php get_footer(); ?>
