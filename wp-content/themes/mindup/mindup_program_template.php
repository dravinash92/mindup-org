<?php
/**
 * Template Name: Mindup Program Page Template
 *
 * Displays the Mindup Program Page
 */
get_header(); ?>

<!-- Header : BEGIN -->
<header class="container-fluid padded-bottom-80 header-pages">
		
</header>
<!-- End : BEGIN -->
    
<!-- Main Page : BEGIN -->
    <main class="container-fluid">
        <article class="row">
            <section class="col-12 padded-top-65 header-video h-100 ">
                <div class="container-lg">
                    <div class="row h-100">
                        <div class="col-12 h-100">
                            <div class="embed-responsive embed-responsive-16by9 h-100">
                                <video class="border_radius embed-responsive-item h-100" preload="auto" poster="<?php the_field('banner_video_thumbnail'); ?>" autoplay loop>
                                    <source src="<?php the_field('banner_video'); ?>" type="video/mp4">
                                </video>
                            </div>
                        </div>

                    </div>
                </div>
        </section>
        <section class="w-100 position-relative top-mint-wave bg-mint program_section">
                <div class="container-md">
                    <div class="row align-content-center">
                        <h1 class="col-12 grad-border left sans-bold program_section_heading"><?php the_field('mindup_title'); ?></h1>
                        <div class="col-12 col-md-6 d-flex flex-column align-self-center">
                            <h5 class="sans-bold program_section_para"><?php the_field('mindup_heading'); ?></h5>
                        </div>
                        <!-- brain asset  -->
                        <div class="col-12 col-md-6 d-flex flex-column align-self-center">
                            <img class="img-fluid" src="<?php the_field('mindup_image'); ?>">
                        </div>
                    </div>
                </div>
            </section>
            
            <section class="w-100 position-relative bg-magenta pillar-blocks ">
                <div class="container-md padded-bottom-80">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <h4 class="grad-border left white text-white pillar-blocks_heading"><?php the_field('pillars_heading'); ?></h4>
                        </div>
                    </div>
                    <div class="row mission-blocks pt-3 h-100 align-items-stretch">
                        <div class="col-12 col-md-6 padded-bottom-40 align-self-stretch d-flex">
                            <div class="mission-block bg-white">
                            
                                <h6 class="sans-bold d-flex align-items-center mindup_program_header"><span class="icon-neuroscience-88x88">
                                </span> <?php the_field('card_title_1'); ?></h6>
                                <p class="sans mb-0 mindup_program_para px-2"><?php the_field('card_description_1'); ?></p>
                                
                            </div>
                        </div>
                        <div class="col-12 col-md-6 padded-bottom-40 align-self-stretch d-flex">
                            <div class="mission-block bg-white">
                            
                                <h6 class="sans-bold d-flex align-items-center mindup_program_header"><span class="icon-mindfulawareness-88x88">
                                </span> <?php the_field('card_title_2'); ?></h6>
                                <p class="sans mb-0 mindup_program_para px-2"><?php the_field('card_description_2'); ?></p>
                                
                            </div>
                        </div>
                        <div class="col-12 col-md-6 padded-bottom-40 align-self-stretch d-flex">
                            <div class="mission-block bg-white">
                                <h6 class="sans-bold d-flex align-items-center mindup_program_header"><span class="icon-positivepsych-88x88">
                                </span> <?php the_field('card_title_3'); ?></h6>
                                <p class="sans mb-0 mindup_program_para px-2"><?php the_field('card_description_3'); ?></p>
                                
                            </div>
                        </div>
                        <div class="col-12 col-md-6 padded-bottom-40 align-self-stretch d-flex">
                            <div class="mission-block bg-white">
                                <h6 class="sans-bold d-flex align-items-center mindup_program_header"><span class="icon-socialemolearning-88x88">
                                </span> <?php the_field('card_title_4'); ?></h6>
                                <p class="sans mb-0 mindup_program_para px-2"><?php the_field('card_description_4'); ?></p>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="w-100 bg-white position-relative brain-section pillars-section">
                <div class="container-md">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <h5 class="sans-bold text-center mind-up-research-pill"><?php the_field('benefits_heading'); ?></h5>
                        </div>
                        <div class="col-12">
                            <div class="d-flex pillar-section_wrapper">
                                <img class="img-fluid pillars-section_img" src="<?php the_field('features_icon_1'); ?>">
                                <p class="sans pillars-section_para"><?php the_field('features_content_1'); ?></p>
                            </div>
                            <div class="d-flex pillar-section_wrapper">
                            <img class="img-fluid pillars-section_img" src="<?php the_field('features_icon_2'); ?>">
                            <p class="sans pillars-section_para"><?php the_field('features_content_2'); ?></p>
                            </div>
                            <div class="d-flex pillar-section_wrapper">
                            <img class="img-fluid pillars-section_img" src="<?php the_field('features_icon_3'); ?>">
                            <p class="sans pillars-section_para"><?php the_field('features_content_3'); ?></p>
                            </div>
                            <div class="d-flex pillar-section_wrapper">
                            <img class="img-fluid pillars-section_img" src="<?php the_field('features_icon_4'); ?>">
                            <p class="sans pillars-section_para"><?php the_field('features_content_4'); ?></p>
                            </div>
                            <div class="d-flex pillar-section_wrapper">
                            <img class="img-fluid pillars-section_img" src="<?php the_field('features_icon_5'); ?>">
                            <p class="sans pillars-section_para"><?php the_field('features_content_5'); ?></p>
                            </div>
                            <div class="d-flex pillar-section_wrapper">
                            <img class="img-fluid pillars-section_img" src="<?php the_field('features_icon_6'); ?>">
                            <p class="sans pillars-section_para"><?php the_field('features_content_6'); ?></p>
                        </div>
                        <div class="col-12 col-lg-11">
							<p class="text-center footnote">(<?php the_field('features_address'); ?>).</p>
						</div>
                    </div>
                </div>
            </section>
            <section class="col-12 bg-mint padded-65">
                
                <div class="container-md">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="grad-border left"><?php the_field('our_program_heading'); ?></h2>
                        </div>
                    </div>
                    <div class="row new-founder-columns row-cols-1 logo-cards row-cols-md-2 h-100">
                        
                        <div id="logo-cards-0" class="col card-col ">
                            <div class="card h-100 px-5">
                                <img src="<?php the_field('logo_1'); ?>" alt="" class="card-img-top p-sm-5 p-md-4">
                                
                                
                                <div class="card-footer mt-3">
                                    
                                    <!-- All other Card link -->
                                    <p class="text-center"><a href="<?php the_field('button_link_1'); ?>" target="_blank" class="btn btn-primary btn-grad-1" title="What's to Come"><?php the_field('button_label_1'); ?></a></p>
                                </div>
                            </div>
                        </div>
                        <div id="logo-cards-1" class="col card-col ">
                            <div class="card h-100 px-5">
                                <img src="<?php the_field('logo_2'); ?>" alt="" class="card-img-top p-5 p-md-4">
                                
                                
                                <div class="card-footer mt-3">
                                    
                                    <!-- All other Card link -->
                                    <p class="text-center"><a href="<?php the_field('button_link_2'); ?>" target="_blank" class="btn btn-primary btn-grad-1" title="What's to Come"><?php the_field('button_label_2'); ?></a></p>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
            </section>
            <section class="col-12 padded-80">
                <div class="container-md padded-top-65">
                    <div class="row padded-bottom-40">
                        <div class="col-12">
                            <h2 class="grad-border left"><?php the_field('training_heading'); ?></h2>
                        </div>
                    </div>

                    <div class="row new-founder-columns row-cols-1 header-cards row-cols-md-2 h-100">
                        
                        <div id="header-cards-0" class="col card-col ">
                            <div class="card h-100">
                                <div class="card-body"><p class="text-center card-text header"><?php the_field('training_title_1'); ?></p></div>
                                
                                
                                
                                <div class="card-footer">
                                    
                                    <!-- All other Card link -->
                                    <p class="text-center"><a href="<?php the_field('training_button_link_1'); ?>" target="_blank" class="btn btn-primary btn-grad-1" title="What's to Come"><?php the_field('training_button_label_1'); ?></a></p>
                                </div>
                            </div>
                        </div>
                        <div id="header-cards-1" class="col card-col">
                            <div class="card h-100">
                                <div class="card-body"><p class="text-center card-text header"><?php the_field('training_title_2'); ?></p></div>
                                
                                <div class="card-footer">
                                    
                                    <!-- All other Card link -->
                                    <p class="text-center"><a href="<?php the_field('training_button_link_2'); ?>" target="_blank" class="btn btn-primary btn-grad-1" title="What's to Come"><?php the_field('training_button_label_2'); ?></a></p>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
            </section>

        </article>
    </main>
<!-- Main Page : END -->
<?php get_footer(); ?>
