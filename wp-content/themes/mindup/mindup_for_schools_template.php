<?php
/**
 * Template Name: Mindup for Schools Page Template
 *
 * Displays the Mindup for Schools Page
 */
get_header(); ?>


<!-- Header : BEGIN -->
<header class="container-fluid padded-bottom-80 header-pages">
		<div class="row">
			<div class="col-12 justify-content-center d-flex">
				<div class="for-schools">
                </div>
				
			</div>
		</div>		
	</header>
<!-- Header : END -->


<!-- Main Page : BEGIN -->
	<main class="container-fluid">
		<article class="row">

			<section class="col-12 top-mint-wave bg-mint padded-top-40 padded-bottom-65">
				<div class="container-md">
					<div class="row align-content-center">
						<div class="col-12">
							<h1 class="grad-border left"><?php the_field('program_heading'); ?></h1>
						</div>
						<div class="col-12 col-md-6 d-flex flex-column align-self-center ">
							<h5><?php the_field('program_title'); ?></h5>
							<p class="pt-3 pb-5"><?php the_field('program_description'); ?></p>
							
						</div>
						<!-- brain asset  -->
						<div class="col-12 col-md-6 d-flex flex-column justify-content-center align-items-center ">
							<img class="img-fluid bubble-img" src="<?php the_field('program_image'); ?>">
						</div>
					</div>
				</div>
			</section>

			<section class="col-12 padded-80">
				<h5 class="text-center padded-bottom-40"><?php the_field('culture_heading'); ?></h5>
				<div class="container-md">
					<div class="row row-cols-1 row-cols-md-3 justify-content-center h-100">
						<div class="col">
							<p class="text-center"><span class="icon-children-82x82">
                            </span></p>
							<p class="text-center member-blurb"><?php the_field('cultural_description_1'); ?></p>
						</div>
						<div class="col">
							<p class="text-center"><span class="icon-school-82x82">
                            </span></p>
							<p class="text-center member-blurb"><?php the_field('cultural_description_2'); ?></p>
						</div>
						<div class="col">
							<p class="text-center"><span class="icon-checklist-82x82">
                            </span></p>
							<p class="text-center member-blurb"><?php the_field('cultural_description_3'); ?></p>
						</div>
						<div class="col">
							<p class="text-center"><span class="icon-lotus-82x82">
                            </span></p>
							<p class="text-center member-blurb"><?php the_field('cultural_description_4'); ?></p>
						</div>
						<div class="col">
							<p class="text-center"><span class="icon-notebook-82x82">
                            </span></p>
							<p class="text-center member-blurb"><?php the_field('cultural_description_5'); ?></p>
						</div>
					</div>

				</div>
			</section>



			<section class="col-12 bg-mint padded-80">
				<div class="container-md">
					<div class="row justify-content-center">
						<div class="col-12 col-md-6 justify-content-center d-flex pb-5 pb-md-0">
							<img src="<?php the_field('casel_program_image'); ?>" class="img-fluid bubble-img" />
						</div>
						<div class="col-12 col-md-6 d-flex flex-column align-self-center">
							<h5 class="text-center text-md-left"><?php the_field('casel_program_title'); ?></h5>
							<p class="sans"><?php the_field('casel_program_description'); ?></p>
						</div>
						
					</div>
				</div>
			</section>

			<section class="col-12 bg-teal padded-80">
				<h4 class="text-center padded-bottom-40"><?php the_field('benefits_heading'); ?></h4>
				<div class="container-md">
					<div class="row mission-blocks pt-3 h-100 align-items-stretch">
						<div class="col-12 col-md-6 padded-bottom-40 align-self-stretch d-flex">
							<div class="mission-block bg-white">
								<h5 class="sans-bold text-tealAlt"><?php the_field('benefits_title_1'); ?></h5>
								<p class="sm-body"><?php the_field('benefits_description_1'); ?></p>
								
							</div>
						</div>
						<div class="col-12 col-md-6 padded-bottom-40 align-self-stretch d-flex">
							<div class="mission-block bg-white">
								<h5 class="sans-bold text-tealAlt"><?php the_field('benefits_title_2'); ?></h5>
								<p class="sm-body"><?php the_field('benefits_description_2'); ?></p>
								
							</div>
						</div>
						<div class="col-12 col-md-6 padded-bottom-40 align-self-stretch d-flex">
							<div class="mission-block bg-white">
								<h5 class="sans-bold text-tealAlt"><?php the_field('benefits_title_3'); ?></h5>
								<p class="sm-body"><?php the_field('benefits_description_3'); ?></p>
								
							</div>
						</div>
						<div class="col-12 col-md-6 padded-bottom-40 align-self-stretch d-flex">
							<div class="mission-block bg-white">
								<h5 class="sans-bold text-tealAlt"><?php the_field('benefits_title_4'); ?></h5>
								<p class="sm-body"><?php the_field('benefits_description_4'); ?></p>
								
							</div>
						</div>
					</div>
				</div>
			</section>


			<section class="col-12 bg-grad-50-teal padded-top-40 padded-bottom-40">
				<div class="container-md">
					<div class="row justify-content-center">
						<div class="col-11 blockquote-container">
							<blockquote><?php the_field('feedback_quotes'); ?></blockquote>
							<p class="attribute"><?php the_field('quotes_by'); ?></p>
						</div>
					</div>
				</div>
			</section>

			<section class="col-12 padded-80">
				<div class="container-md padded-bottom-65">
					<div class="row justify-content-center padded-bottom-80">
						<div class="col-12 col-md-6 d-flex flex-column align-self-center">
							<h5><?php the_field('training_title'); ?></h5>
							<p class="sans sm-body"><?php the_field('training_description'); ?></p>
							<p class="text-center text-md-left"><a class="btn btn-light btn-grad-1 mx-0" href="<?php the_field('training_button_link'); ?>"><?php the_field('training_button_label'); ?></a></p>
						</div>
						<div class="col-12 col-md-6 justify-content-center align-items-center d-flex">
							<img src="<?php the_field('training_image'); ?>" class="img-fluid bubble-img" />
						</div>
						
					</div>

					<div class="row bg-magenta justify-content-center padded-40 rounded-lg">
						<div class="col-10">
							<h6 class="text-center text-white padded-bottom-40"><?php the_field('join_community_title'); ?></h6>
							<p class="text-center text-white"><?php the_field('join_community_description'); ?></p>
							<p class="text-center"><a class="btn btn-light btn-white" href="<?php the_field('join_button_link'); ?>"><?php the_field('join_button_label'); ?></a></p>
						</div>
					</div>
				</div>
			</section>

		</article>
	</main>
<!-- Main Page : END -->
<?php get_footer(); ?>
