<?php
/**
 * Template Name: Home Page Template
 *
 * Displays the Home Page
 */
get_header(); ?>
    <!-- Header : BEGIN -->
	<header class="container-fluid padded-80 bg-mint header-home">
		<div class="row">
			<div class="col-12">
				<div class="container-lg">
					<div class="row justify-content-center align-content-center">
						<div class="col-12 col-sm-9 col-md-6 d-flex flex-column align-self-center ">
							<h1 class=""><?php the_field('banner_title'); ?></h1>
							<p  class=""><?php the_field('banner_description'); ?></p>
							<p class="text-left "><a href="<?php the_field('button_link'); ?>" class="btn btn-primary btn-grad-1 mx-0"><?php the_field('button_label'); ?></a></p>
						</div>
						<!-- brain asset  -->
						<div class="col-8 col-md-6 d-flex flex-column align-self-center">
							<div class="embed-responsive embed-responsive-1by1">
								
								<video id="mintVideo"  class="border_radius embed-responsive-item" poster="<?php the_field('banner_image'); ?>" autoplay muted>
									<source src="<?php the_field('banner_video_url'); ?>" type="video/mp4">
								</video>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</header>
<!-- End : BEGIN -->

<!-- Main Page : BEGIN -->
	<main class="container-fluid">
		<article class="row">

			<section class="col-12 padded-80">
				<h2 class="text-center grad-border center pb-5"><?php the_field('why_we_do_heading'); ?></h2>
				<div class="container-md">
					<div class="row ">
						<div class="col-12 col-md-5 col-lg-4  goldie-section d-flex flex-column justify-content-center align-items-center justify-content-md-start justify-content-lg-center mb-3 ">
							<div class="goldie-headshot"></div>
							<div class="goldie-attr-border">
								<div class="goldie-attr">
									<p class="name sans-bold text-center"><?php the_field('profile_title'); ?></p>
									<p class="title sans text-center"><?php the_field('profile_designation'); ?></p>
								</div>
							</div>
						</div>  
						<div class="col-12 col-md-7 col-lg-8  d-flex flex-column align-self-center">
							<h4 class="text-prim mb-3 text-why"><?php the_field('our_title'); ?></h4>
							<p class="content"><?php the_field('our_description'); ?></p>
							<p class="text-center text-md-left mt-4"><a href="<?php the_field('our_button_link'); ?>" class="btn btn-primary btn-grad-1 mx-0 "><?php the_field('our_button_label'); ?></a></p>
						</div>
					</div>
				</div>
			</section>

			<section class="col-12 bg-grad-50-magenta">
				<div class="container-md">
					<div class="row justify-content-center">
						<div class="col-11 blockquote-container">
							<blockquote><?php the_field('review_quotes'); ?></blockquote>
							<p class="attribute"><?php the_field('review_by'); ?></p>
						</div>
					</div>
				</div>
			</section>

			<section class="w-100 padded-80 bg-magenta educational-section ">
				<div class="container-md">
					<div class="row justify-content-center">
						<div class="col-12">
							<h4 class="text-white text-center pb-3"><?php the_field('foster_description'); ?></h4>
						</div>
						<div class="col-6 col-md-3">
							<p class="text-white text-center"><span class="icon-neuroscience-88x88"></span></p>
							<p class="text-center text-white"><?php the_field('foster_title_1'); ?></p>
						</div>
						<div class="col-6 col-md-3">
							<p class="text-white text-center"><span class="icon-mindfulawareness-88x88"></span></p>
							<p class="text-center text-white"><?php the_field('foster_title_2'); ?></p>
						</div>
						<div class="col-6 col-md-3">
							<p class="text-white text-center"><span class="icon-positivepsych-88x88"></span></p>
							<p class="text-center text-white"><?php the_field('foster_title_3'); ?></p>
						</div>
						<div class="col-6 col-md-3">
							<p class="text-white text-center"><span class="icon-socialemolearning-88x88"></span></p>
							<p class="text-center text-white"><?php the_field('foster_title_4'); ?></p>
						</div>
					</div>
				</div>
			</section>
			
			

			<section class="col-12 bg-white position-relative brain-section chart-section">
				<div class="container-md">
					<div class="row justify-content-center">
						<div class="col-10">
							<h2 class="text-center grad-border center padding-top-65"><?php the_field('evidence_heading'); ?></h2>
							<h5 class="text-center text-prim mb-4"><?php the_field('evidence_title'); ?></h5>
						</div>
						<div class="col-12 col-md-4 chart-value d-flex flex-column justify-content-start  align-items-center">
							<div class="eighty-six d-flex">
								<p class="chart-number align-self-center">86%</p>
							</div>
							<p class="text-center mt-3 mb-4 chart-text mx-5 mx-md-0 "><?php the_field('progress_description_1'); ?></p>
						</div>
						<div class="col-12 col-md-4 chart-value d-flex flex-column justify-content-start  align-items-center">
							<div class="eighty-eight d-flex">
								<p class="chart-number align-self-center">88%</p>
							</div>
							<p class="text-center mt-3 mb-4 chart-text mx-5 mx-md-0 "><?php the_field('progress_description_2'); ?></p>
						</div>
						<div class="col-12 col-md-4 chart-value d-flex flex-column justify-content-start  align-items-center">
							<div class="eighty-three d-flex">
								<p class="chart-number align-self-center">83%</p>
							</div>
							<p class="text-center mt-3 mb-4 chart-text mx-5 mx-md-0 "><?php the_field('progress_description_3'); ?></p>
						</div>

						<div class="col-12 text-center padded-40">
							<a href="<?php the_field('evidence_button_link'); ?>" class="btn btn-primary btn-grad-1"><?php the_field('evidence_button_label'); ?></a>
						</div>

						<div class="col-12 col-lg-11">
							<p class="text-center footnote"><?php the_field('evidence_address'); ?></p>
						</div>
						
					</div>
				</div>
			</section>

			<section class="w-100 position-relative padded-40 bg-blue top-blue-wave sel-section">
				<div class="container h-100">
					<div class="row justify-content-center align-items-center h-100">
						<div class="col-12 col-md-6">
						<div class="sel-bubble-img"></div>
						</div>
						<div class="col-12 col-md-6">
							<h5 class="text-white sans-demi"><?php the_field('social_description'); ?></h5>
						</div>						
					</div>
				</div>
			</section>

			<section class="w-100 position-relative padded-80 bg-skyA">
				<div class="container-md">
					<div class="row justify-content-start align-items-center align-items-md-start">
						<div class="col-12 col-md-6 mb-4 mb-md-0">
							<h5 class="pb-2"><?php the_field('casel_title'); ?></h5>
							<p class="sans"><?php the_field('casel_description'); ?></p>
						</div>
						<div class="col-12 col-md-6 justify-content-center d-flex">
							<img src="<?php the_field('casel_image'); ?>" class="img-fluid casel-img" />
						</div>
					</div>
				</div> 
			</section>

			<section class="col-12 padded-80">
				<h2 class="text-center grad-border center"><?php the_field('membership_title'); ?></h2>
				<h5 class="text-center text-prim padded-bottom-65 pt-2"><?php the_field('membership_sub_title'); ?></h5>
				<div class="container-md membership-cards">
					<div class="row no-gutters row-cols-1 row-cols-md-3">
					<?php $pricing = array('post_type' => 'pricing','post_status'=>'publish','order' => 'ASC');
						$pricings = new WP_Query($pricing);
						if($pricings->have_posts()) : while ($pricings->have_posts()) : $pricings->the_post(); ?>
							<div class="col pr-md-2 pr-lg-3 mb-4">
								<div class="card h-100 <?php the_field('color_code'); ?> border-0 membership-cards">
									<div class="card-header bg-transparent border-0">
										<h4 class="text-center text-white card-title"><?php the_field('pricing_name'); ?></h4>
										<div class="text-center">
										<img src="<?php the_field('pricing_image'); ?>" class="card-img" height="82" style="width: 82px !important;">
										</div>
									</div>
									<div class="card-body">
										<h5 class="text-center text-white">Access To:</h5>
										<div clas="text-left">
											<?php the_field('pricing_features'); ?>
										</div>
									</div>
									<div class="card-footer text-center bg-transparent border-0">
										<a href="<?php the_field('pricing_button_link'); ?>" class="btn btn-light btn-white"><?php the_field('pricing_button_label'); ?></a>
									</div>
								</div>
							</div>
						<?php endwhile;
                        endif; wp_reset_query(); ?>
					</div>
					<div class="row padded-top-40">
						<div class="col-12 text-center">
							<a href="<?php the_field('membership_button_link'); ?>" class="btn btn-primary btn-grad-1"><?php the_field('membership_button_label'); ?></a>
						</div>
					</div>
				</div>
			</section>

			<section class="col-12 bg-grad-50-yellow">
				<div class="container-md">
					<div class="row justify-content-center">
						<div class="col-11 blockquote-container">
							<blockquote><?php the_field('review_quotes_2'); ?></blockquote>
							<p class="attribute"><?php the_field('review_by_2'); ?></p>
						</div>
					</div>
				</div>
			</section>

			<section class="col-12 bg-yellow padded-80">
				<h2 class="text-center grad-border center"><?php the_field('mindup_news_heading'); ?></h2>
				<div class="container">
					<div class="row new-founder-columns row-cols-1 video-cards row-cols-md-3 h-100">
					<?php 
						$news_posts = get_field('news_order');
						
						if( $news_posts ): ?>
							
							<?php foreach( $news_posts as $news_post ): ?>
								<div id="video-cards-0" class="col card-col justify-content-center d-flex d-md-block">
									<div class="card h-100">
										<a href="#videoModal" role="button" class="video-header-container" data-toggle="modal" data-target="#news<?php echo $news_post->ID ?>" title="MindUP animation FPO">
											<div style="background-image: url(<?php the_field('news_video_thumbnail',$news_post->ID); ?>);" class="card-img-top" aria-label=""></div>
										</a>
										<div class="card-body">
											<p class="card-text"><?php the_field('news_content', $news_post->ID); ?></p>
										</div>								
											
										<div class="card-footer">
											<p class="video-cta"><a href="#news<?php echo $news_post->ID ?>" role="button" class="video-header-container" data-toggle="modal" data-target="#news<?php echo $news_post->ID ?>" title="MindUP animation FPO">Watch the Video</a></p>
										</div>
									</div>
								</div>

							<?php endforeach; ?>
							
						<?php endif; ?>
					</div>
					<?php 
						$news_posts = get_field('news_order');
						
						if( $news_posts ): ?>
							
						<?php foreach( $news_posts as $news_post ): ?>
						<div class="modal fade " id="news<?php echo $news_post->ID ?>" tabindex="-1"  aria-labelledby="videoModalLabel" aria-hidden="true" >
						    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						    	<span class="close-icon icon-remove-34x34 text-white" id="closeVideo"  aria-hidden="true"></span>
						    </button>
						  	<div class="modal-dialog modal-xl modal-dialog-centered">					          
						        <div class="modal-content">
						            <div class="modal-body video-body">
						            <!-- popup video -->
						                <div class="embed-responsive embed-responsive-16by9">
						                	<div class="embed-responsive-item">
											<?php $upload_video = get_field('news_video',$news_post->ID);?>
												<?php if($upload_video) { ?>
													<video id="modalVid" class="embed-responsive-item" crossorigin="Anonymous" preload="none" poster="<?php the_field('news_video_thumbnail',$news_post->ID); ?>" controls>
						  								<source src="<?php the_field('news_video',$news_post->ID); ?>" type="video/mp4">
						      						</video>
												<?php } 
												else { ?>
													<iframe class="embed-responsive-item" src="<?php the_field('news_social_video',$news_post->ID); ?>"></iframe>
												<?php } ?>
						      					<img src="<?php echo get_template_directory_uri(); ?>/lib/imgs/ui/icon/video-50x50.svg" class="play_icon" alt="">
						      				</div>
						         		</div>
						            </div>
						        </div>
							</div>
						</div>
						<?php endforeach; ?>
							
						<?php endif; ?>
				</div>
				<p class="text-center"><a href="<?php echo get_site_url(); ?>/news" class="btn btn-primary btn-grad-1">View More News</a></p>
			</section>
		</article>
	</main>
<!-- Main Page : END -->
<?php get_footer(); ?>
