<?php
/**
 * Template Name: Members Detail Page Template
 *
 * Displays the Members Detail Page
 */
get_header(); ?>


<!-- Header : BEGIN -->
<header class="container-fluid padded-bottom-80 header-pages">
		<div class="row">
			<div class="col-12 justify-content-center d-flex">

				
			</div>
		</div>		
	</header>
<!-- Header : END -->


<!-- Main Page : BEGIN -->
	<main class="container-fluid">
		<article class="row">


		 		<section id="expert-section" class="col-12 padded-80  ">
		 			<div class="container-md">
				 		<div class="row pb-4 align-items-center title-back">
				 			<div class="col-12 col-md-4 pb-3 pb-md-0 d-flex justify-content-center">
				 				
				 				<div class="expert-head" style="background-image: url(<?php echo get_template_directory_uri(); ?>/lib/imgs/advisory/biopics-KimSchonertReichl@2x.jpg);"></div>
				 			</div>
				 			<div class="col-12 col-md-8">
				 				<h1>Kim Schonart-Reichl, Ph.D.</h1>
				 				

				 			</div>
				 		</div>
				 		<div class="row">
			 			<div class="col-12 col-md-4 pb-5 pb-md-0">
			 				<p class="expert-side sans-demi mb-0">Expert:</p>
			 				<p class="expert-side sans-demi text-prim">Social and Emotional Learning</p>
			 				<ul class="expertise-list">
			 					<li>Applied Developmental Psychologist</li>
								<li>NoVO Foundation Endowed Chair of Social and Emotional Learning</li>
								<li>University of Illinois at Chicago.</li>
							</ul>
			 				
			 			</div>
			 			<div class="col-12 col-md-8 pt-3 pt-md-0">
			 				<h3 class="text-prim pb-4">About</h3>
			 				<p>Dr. Kimberly A. Schonert-Reichl is the NoVo Foundation Endowed Chair in Social and Emotional Learning in the Department of Psychology at the University of Illinois at Chicago. From 1991 to 2020, she was a Professor in the Department of Educational and Counseling Psychology, and Special Education in the Faculty of Education at University of British Columbia (UBC).</p>

							<p>Prior to her graduate work, Dr. Schonert-Reichl worked as middle school teacher and then as a teacher at an alternative high school for adolescents identified as at risk for high school completion.</p>

							<p>Known as a world renowned expert in the area of social and emotional learning (SEL), Dr. Schonert-Reichl’s research focuses on identification of the processes that foster positive human qualities such as empathy, compassion, altruism, and resiliency in children and adolescents. Her projects in this area include studies examining the effectiveness of classroom-based universal SEL programs including including MindUP. Dr. Schonert-Reichl has over 150 publications in scholarly journals, book chapters, and reports, and has edited two books on mindfulness in education. She has presented over 300 research papers at scholarly conferences and has given over 400 presentations on the topic of children’s social and emotional development and SEL to lay audiences, including parents, community organizations, educators, and policy makers. She is the recipient of the 2021 Janusz Korczak Medal for Children’s Rights Advocacy, the 2019 Postsecondary Leader of the Year Award – Canadian Edtech Awards, the 2015 Joseph E. Zins Distinguished Scholar Award for outstanding research on SEL, and the 2009 Confederation of University Faculty Associations BC’s Paz Buttedahl Career Achievement Award in recognition of sustained outstanding contributions to the community beyond the academy through research over the major portion of a career.</p>

							<p>Dr. Schonert-Reichl has been involved with many scholarly committees and consultancies. She serves as an advisor to the British Columbia (BC) Ministry Education on the development and implementation of the redesign of the Curriculum and Assessment Framework that focuses on the promotion of students’ personal and social competencies; an Expert Advisor to the Organization for Economic Co-Operation and Development’s (OECD) Education 2030 initiative, a Board Member of the Collaborative for Academic, Social, and Emotional Learning (CASEL), an advisor to UNESCO’s Mahatma Gandhi Institute of Education for Peace and Sustainable Development (MGIEP) on SEL.</p>

							<p>Dr. Schonert-Reichl’s research has been highlighted in several magazines and newspapers across Canada, the US, and internationally, including The New York Times, The Washington Post, Le Monde, The Wall Street Journal, Time Magazine, Scientific American Mind, Neurology Now, The Huffington Post, The Telegraph, The Atlantic, The Daily Mail, The Los Angeles Times, US News, The National Post, Canadian Living Magazine, Reader’s Digest – Canada, The Greater Good, The Toronto Star, The Globe and Mail, The Vancouver Sun, 24, and Today’s Parents.</p>
							<p><span class="sans-bold">For more information, see Kim’s website at:</span><br /> 
							<a href="http://sel.ecps.educ.ubc.ca/" target="_blank">http://sel.ecps.educ.ubc.ca/</a></p>
			 			</div>
			 		</div>
			 	</section>




		</article>
	</main>
<!-- Main Page : END -->
<?php get_footer(); ?>
