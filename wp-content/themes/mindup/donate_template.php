
<?php
/**
 * Template Name: Donate Page Template
 *
 * Displays the Donate Page
 */
get_header(); ?>

<!-- Header : BEGIN -->
<header class="container-fluid padded-bottom-80 header-pages">
			
</header>
<!-- Header : END -->
        
        
<!-- Main Page : BEGIN -->
    <main class="container-fluid">
        <article class="row">

            <section class="col-12 padded-80 ">
                <div class="container-md">
                    <div class="row justify-content-center ">
                        <div class="col-12">
                            <h1 class="grad-border left "><?php the_field('banner_heading'); ?></h1>
                        </div>
                        <div class="col-11 rounded-cta bg-greyultralt d-flex flex-column justify-content-center align-items-center ">
                            <p><a href="<?php the_field('donate_button_link'); ?>" class="btn btn-primary btn-grad-1"><?php the_field('donate_button_label'); ?></a></p>
                            <div class="row no-gutters justify-content-center align-items-center px-2">
                                <div class="col donate-logos d-flex flex-row justify-content-around align-items-center">
                                    <div class="cc-comp visa"></div>
                                    <div class="cc-comp mc"></div>
                                    <div class="cc-comp disc"></div>
                                    <div class="cc-comp amx"></div>
                                    <div class="cc-comp pp"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="col-12 padded-40 bg-grad-50-blue ">
                <div class="container-md">
                    <div class="row justify-content-center ">
                        <div class="col-12 d-flex justify-content-center ">
                            <img class="img-fluid" src="<?php the_field('banner_image'); ?>" />
                        </div>
                    </div>
                </div>
            </section>

            <section class="col-12 padded-top-65 padded-bottom-80 bg-blue ">
                <div class="container-md">
                    <div class="row justify-content-center ">
                        <div class="col-12">
                            <h3 class="text-mint text-center pb-3"><?php the_field('banner_title'); ?></h3>
                            <h5 class="text-white text-center padded-bottom-65"><?php the_field('banner_description'); ?></h5>
                        </div>
                    </div>
                    <div class="row row-cols-1 row-cols-md-3 justify-content-center h-100">
                        <div class="col">
                            <p class="text-center mb-0"><span class="text-mint icon-children-82x82"></span></p>
                            <h4 class="text-mint text-center"><?php the_field('population_count_1'); ?></h4>
                            <p class="text-center text-white lg-body pb-4"><?php the_field('population_content_1'); ?></p>
                        </div>
                        <div class="col">
                            <p class="text-center mb-0"><span class="text-mint icon-apple-82x82"></span></p>
                            <h4 class="text-mint text-center"><?php the_field('population_count_2'); ?></h4>
                            <p class="text-center text-white lg-body pb-4"><?php the_field('population_content_2'); ?></p>
                        </div>
                        <div class="col">
                            <p class="text-center mb-0"><span class="text-mint icon-world-82x82"></span></p>
                            <h4 class="text-mint text-center"><?php the_field('population_count_3'); ?></h4>
                            <p class="text-center text-white lg-body pb-4"><?php the_field('population_content_3'); ?></p>
                        </div>						
                    </div>
                </div>
            </section>

            <section class="col-12 padded-80 bg-mint bottom-mint-wave">
                <div class="container-md">
                    <div class="row  ">
                        <div class="col-12 col-md-6 d-flex flex-column align-self-center">
                            <h4><?php the_field('work_title'); ?></h4>
                            <p class="lg-body"><?php the_field('work_description'); ?></p>
                            <p class="text-left"><a href="<?php the_field('work_button_link'); ?>" class="btn btn-primary btn-grad-1 mx-0"><?php the_field('work_button_label'); ?></a></p>
                        </div>
                        <div class="col-12 col-md-6 d-flex flex-column align-items-center">
                            <img class="img-fluid bubble-img" src="<?php the_field('work_image'); ?>">
                        </div>
                    </div>
                </div>
            </section>

            <section class="col-12 padded-80 ">
                <div class="container-md">
                    <div class="row justify-content-center ">
                        <div class="col-12">
                            <h5 class="text-center"><?php the_field('payment_title'); ?></h5>
                            <h3 class="text-center text-prim padded-top-65 padded-bottom-40 "><?php the_field('payment_sub_title'); ?></h3>
                        </div>
                        <div class="col-11 rounded-cta bg-white	 d-flex flex-column justify-content-center align-items-center ">
                            <p><a href="<?php the_field('donate_button_link'); ?>" class="btn btn-primary btn-grad-1"><?php the_field('donate_button_label'); ?></a></p>
                            <div class="row no-gutters justify-content-center align-items-center px-2">
                                <div class="col donate-logos d-flex flex-row justify-content-around align-items-center">
                                    <div class="cc-comp visa"></div>
                                    <div class="cc-comp mc"></div>
                                    <div class="cc-comp disc"></div>
                                    <div class="cc-comp amx"></div>
                                    <div class="cc-comp pp"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </article>
    </main>
<!-- Main Page : END -->
<?php get_footer(); ?>