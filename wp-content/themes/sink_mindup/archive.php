<?php 	get_header();
		$archive = parse_archive_meta();
 ?>
 <!-- archive.php : BEGIN -->
<main class="onepage" id="maincontent" role="main">
	<section class="content">
		<header class="pageinfo">
			<h1><?php echo $archive->title ?></h1>
		</header>
		<?php 	if (have_posts()) {
			$i=0;
			global $post;
			while (have_posts()) {
				$i++;
				the_post();
				$item = $post;
				get_single_archive($item, $i, $wp_query->post_count);
		 } ?>
		<nav class="archive_relatedposts relatedposts">
			<ul>
				<li class="backward prev"><?php previous_posts_link('&laquo; Previous Items') ?></li>
				<li class="forward next"><?php next_posts_link('More Items &raquo;') ?></li>
			</ul>
		</nav>
		<?php } ?>
	</section>
</main>
<!-- archive.php : END -->
<?php get_footer(); ?>