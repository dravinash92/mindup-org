<?php
/*
Template Name: Expert Page
*/
?>
<?php 	get_header();
		global $post;
		$main = $post;
		the_post();
 ?>

 <!-- page-template-expert.php | TEMPLATE: Expert Page : BEGIN -->
	<main class="onepage" id="maincontent" role="main">
		
	 		<article <?php post_class('copy') ?>>
	 			<!-- expert content : BEGIN -->
				 	<div class="title-back">
				 		<section class="container-md px-md-0">
					 		<div class="row pb-4 align-items-center">
					 			<div class="col-12 col-md-4 pb-3 pb-md-0 d-flex justify-content-center">
					 				<?php
					 				/* grab the url for the full size featured image */
        					$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
					 				?>
					 				<div class="expert-head" style="background-image: url(<?php echo $featured_img_url; ?>);"></div>
					 			</div>
					 			<div class="col-12 col-md-8">
					 				<h1><?php the_title(); ?></h1>
					 				

					 			</div>
					 		</div>
					 		<div class="row">
					 			<div class="col-12 col-md-4 pb-5 pb-md-0">

					 				<p class="expert-side sans-demi mb-0">Expert:</p>
					 				<?php	get_expertlist(); ?>
					 				
					 			</div>
					 			<div class="col-12 col-md-8 pt-3 pt-md-0">
					 				<h2 class="large-pink pb-4">About</h2>
					 				<?php the_content(); ?>
					 			</div>
					 	</section>
				 	</div>
				 	<?php	get_cardimporter() ?>
			
			</article>
		
	</main>
<!-- page-template-expert.php | TEMPLATE: Expert Page : END -->
<?php get_footer(); ?>