<?php get_header(); ?>
<?php
global $wp_query;
$s = filter_input(INPUT_GET, 's', FILTER_SANITIZE_SPECIAL_CHARS);
$res_txt = ($wp_query->found_posts > 1) ? ' results' : ' result';
if (have_posts())
	$title = 'Found ' . $wp_query->found_posts . $res_txt . ' for <span class="search_result">\'' .  $s . '\'</span>';
else
	$title = 'Sorry, couldn\'t find anything for <span class="search_result">\'' .  $s . '\'</span>';
?>
<main class="onepage" id="maincontent" role="main">
	<section class="content centering_box">
		<header class="pageinfo">
			<h1><?php echo $title ?></h1>
		</header>
		<?php if (have_posts()) {
			$i=0;
			global $post;
			while (have_posts()) {
				$i++;
				the_post();
				$item = $post;
				get_single_archive($item, $i, $wp_query->found_posts);
				 } ?>
	 <nav class="archive_relatedposts relatedposts">
	 	<ul>
	 		<li class="backward prev"><?php previous_posts_link('&laquo; Previous Items') ?></li>
	 		<li class="forward next"><?php next_posts_link('More Items &raquo;') ?></li>
	 	</ul>
	 </nav>
	<?php } else { ?>
		<div class="text">
			<?php get_notfound_help() ?>
		</div>
	<?php } ?>
	</section>
</main>
<?php get_footer(); ?>