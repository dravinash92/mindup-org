<?php
define('SINK_CLIENT_APP_DEPENDENCY_URL', '_social-ink-app_/dependencies/vendor');

require_once(SINK_CLIENT_APP_DEPENDENCY_URL . '/leafo/scssphp/scss.inc.php');
require_once(SINK_CLIENT_APP_DEPENDENCY_URL . '/leafo/scssphp/example/Server.php');

use Leafo\ScssPhp\Compiler;
use Leafo\ScssPhp\Formatted\Compressed;
use Leafo\ScssPhp\Server;
$directory = "assets/css";
$scss = new Compiler();
$scss->setFormatter('Leafo\ScssPhp\Formatter\Compressed');
//$scss->setSourceMap(Compiler::SOURCE_MAP_FILE);
//$server = new Server($directory);
$server = new Server($directory, null, $scss);
$server->serve();