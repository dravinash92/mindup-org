<?php
/*
Template Name: Page - No Main Section
*/
?>
<?php
	get_header();
	global $post;
	$main = $post;
	the_post();
 ?>
<!-- page-no-main.php : BEGIN -->
<main class="onepage" id="maincontent" role="main">

<?php echo socialink_pagebuilder() ?>
</main>
<?php get_footer(); ?>