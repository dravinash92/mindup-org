<?php
/*
Template Name: NEW Homepage
*/
?>
<?php 	get_header();
		global $post;
		$main = $post;
		the_post();
 ?>

 <!-- page-template-new-homepage.php | TEMPLATE: NEW Homepage : BEGIN -->
 <main class="onepage" id="maincontent" role="main">
	 <article <?php post_class('copy') ?>>
		<?php
			// query free content pages
				// $all_users = array(
				// 	'post_type'				=> 'page',
				// 	'post__in'        => array(),
				// 	'posts_per_page'	=> 3,
				// 	'order'						=> 'ASC',
				// 	'orderby'					=> 'menu_order'
				// );
			

			// query paid content pages : BEGIN
				$paid_users = array(
					'post_type'				=> 'page',
					'post__in'        => array(1376,1400), //local 1318,1265
					'posts_per_page'	=> 2,
					'order'						=> 'ASC',
					'orderby'					=> 'menu_order'
				);

				$paid_pages = new WP_Query( $paid_users );
			// query paid content pages : END
				
		?>

		

			
		
		<!-- Main Home Content : BEGIN -->
		 	<div class="home-back">
		 		<section class="container-md px-md-0">
			 		<div class="row">
			 			<div class="col-12">
			 				<h1><?php the_title(); ?></h1>
			 			
			 				<?php //the_content(); ?>
			 			</div>
			 		</div>
			 	</section>
			 	<!-- logo copy cards : BEGIN -->

			 		<section class="container-md px-md-0">
			 			<?php get_homecardimporter(); ?>
			 		</section>		 	
		 		<!-- logo copy cards : END -->
		 		<?php
			 		if(is_user_logged_in()){
						echo "<!-- Logged in. -->";

						$membershipID_paid = get_page_by_title('Paid Member',OBJECT,'memberpressproduct');
						$membershipID_free = get_page_by_title('Member',OBJECT,'memberpressproduct');

						$mepr_user = new MeprUser( get_current_user_id() );

						if($mepr_user->is_already_subscribed_to( $membershipID_paid->ID )){
							echo "<!-- Paid membership. -->"; ?>
							 		<?php if ( $paid_pages->have_posts() ) : ?>		
							 			<!-- paid header cards CTA : BEGIN --> 			
									 		<section class="container-md px-md-0">
									 			<div class="row new-founder-columns cta header-cards row-cols-1 row-cols-md-2 h-100">

									 				<?php while ( $paid_pages->have_posts() ) : $paid_pages->the_post(); ?>
										 				<div class="col card-col">
										 					<div class="card h-100">
										 						
										 						<div class="card-body">
										 							<p class="card-text header"><?php the_title(); ?></p>
										 							<?php $excerpt = get_the_excerpt(); ?>
										 							<p class="card-text sans-bold"><?php echo $excerpt; ?></p>
										 							
										 						</div>
										 						<div class="card-footer">
										 							<p class="text-center"><a href="<?php the_permalink() ?>" class="btn btn-primary btn-white">Learn More</a></p>
										 						</div>
										 					</div>
										 				</div>
									 				<?php endwhile; ?>				 				
									 			</div>
									 		</section>
									 	<!-- paid header cards CTA : END -->

									<?php
									wp_reset_postdata();
									else : ?>

									<?php endif; ?>	
						<?php } else if($mepr_user->is_already_subscribed_to( $membershipID_free->ID )){
								echo "<!-- Free membership. -->";
						} else {
								//no membership defined
						}
					} else {
						echo "<!-- Not logged in. -->";
					}
				?>		 		
		 	</div>
		<!-- Main Home Content : END -->
		 
	 </article>
	 <?php	get_cardimporter(); ?>

	 <!-- 2 logo cards : BEGIN -->
	 	<div class="no-back">
	 		<section class="container-md px-md-0">
	 			<div class="row new-founder-columns logo-cards two-up row-cols-1 h-100">
	 				<div class="col card-col">
	 						<div class="card h-100">
	 							<div class="two-logos">
	 								<img src="<?php echo get_theme_file_uri() ?>/images/logocard-mindup-for-life-small.jpg" width="170" height="75" class="card-img-top" />
	 								<img src="<?php echo get_theme_file_uri() ?>/images/world-health-institute-logo.jpg" width="170" height="75" class="card-img-top" />
	 							</div>
	 							<div class="card-body">
	 								<p class="card-text text-center">We are grateful to have received support from Whole Health Institute.</p>
	 							</div>
	 							
	 						</div>
	 				</div>
	 			</div>
	 		</section>
	 	</div>
	 <!-- 2 logo cards : END -->


</main>
<!-- page-template-new-homepage.php | TEMPLATE: NEW Homepage : END -->
<?php get_footer(); ?>