<?php
/*
Template Name: Content then Cards then Page Builder
*/
?>
<?php 	get_header();
		global $post;
		$main = $post;
		the_post();
 ?>
<!-- page.php : BEGIN -->
<main class="onepage" id="maincontent" role="main">
<section class="content centering_box">
	<article <?php post_class('copy') ?>>
		<header class="pageinfo">
			<h1><?php the_title() ?></h1>
		</header>
		<div class="text">
			<?php the_content(); ?>
		</div>
	</article>
</section>

<?php	get_cardimporter() ?>

<?php echo socialink_pagebuilder() ?>
<!-- load inc -->


</main>
<?php get_footer(); ?>