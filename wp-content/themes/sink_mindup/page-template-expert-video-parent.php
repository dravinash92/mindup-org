<?php
/*
Template Name: Expert Video Library
*/
?>
<?php 	get_header();
		global $post;
		$main = $post;
		the_post();
 ?>

 <!-- page-template-expert-video-parent.php | TEMPLATE: Expert Video Library : BEGIN -->
 <main class="onepage" id="maincontent" role="main">
	 <article <?php post_class('copy') ?>>
		 

		 <!-- expert content : BEGIN -->
		 	<div class="title-back">
		 		<section class="container-md px-md-0">
			 		<div class="row">
			 			<div class="col-12">
			 				<h1><?php the_title(); ?></h1>
			 			
			 				<?php //the_content(); ?>
			 			</div>
			 		</div>
			 	</section>
		 	</div>
		 	<?php	get_cardimporter(); ?>
	 </article>
	 
</main>
<!-- page-template-expert-video-parent.php | TEMPLATE: Expert Video Library : END -->
<?php get_footer(); ?>