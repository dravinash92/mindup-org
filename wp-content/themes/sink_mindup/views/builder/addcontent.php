<?php
if(!$module->site_content)
    return false; ?>
<div class="module_inner centering_box">
<?php
$module->related_content_type = 'grid';
foreach($module->site_content as $i => $item) {
	if($module->related_content_type == 'grid') {
		echo get_single_archive_grid($item, $i, count($module->site_content));
	} elseif($module->related_content_type == 'list') {
		echo get_single_archive($item, $i, count($module->site_content));
	}
}
?>
</div>