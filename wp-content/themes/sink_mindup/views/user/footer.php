<?php
$userFunctions = new sinkMindUpUser();
$user = $userFunctions->getUser();
?>
<section class="socialink-framework mindup-userfunctions userfunctions-footer mindup-socialink socialink-functions <? echo $userFunctions->userfunctions_footer_classes() ?>">
	<div class="inner flexible">
	<?php	if(!$user) { ?>
			<a href="<?php echo $userFunctions->getUserLogIn() ?>">Log In</a>
			<a href="<?php echo $userFunctions->getUserSignupPage() ?>" class="gradient-default button">Get Started Now</a>
		<?php } ?>
	</div>
</section>