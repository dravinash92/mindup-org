<?php
$oddeven_class = ($i % 2 == 0 ) ? 'even_post' : 'odd_post';
$feat_img = (get_the_post_thumbnail($item->ID)!='') ? wp_get_attachment_image_src( get_post_thumbnail_id($item->ID), $thumbsize) : false;
$thumb_class = $feat_img ? 'thumb_exists' : 'thumb_none';
$count_class = 'archivepost' . $i;
$link = get_permalink($item->ID);
if($i==$total)
	$ordering_class = 'archivepost_last';
elseif($i==1)
	$ordering_class = 'archivepost_first';
else
	$ordering_class = '';
?>
<article <?php post_class("$oddeven_class griditem archive_post $thumb_class $count_class $ordering_class flexible") ?>>
	<?php if($feat_img) { ?>
		<div class="featimg">
			<a href="<?php echo $link ?>" rel="bookmark" title="Permanent Link to <?php echo $item->post_title ?>"><img src="<?php echo $feat_img[0] ?>" alt="<?php echo $item->post_title ?>"/></a>
		</div>
	<?php } ?>
	<header class="postinfo">
		<h2 id="post-<?php echo $item->ID; ?>"><a href="<?php echo $link ?>" rel="bookmark" title="Permanent Link to <?php echo get_the_title($item->ID); ?>"><?php echo str_replace(' - ','<br>',$item->post_title) ?></a></h2>
	</header>
</article>