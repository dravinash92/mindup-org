<?php
$media_options = explode(',',SINK_CLIENT_MEDIAOPTIONS);
if($_POST['submit_form_fields'] == 'Y') {
	foreach($_POST as $key => $value) {
		if($key!="[submit_form_fields]")
			set_sinktheme_option($key, $value);
	}	
	echo '<div class="updated"><p><strong>Social Media options have been updated.</strong></p></div>';
}
?>
<div class="wrap socialink_theme_options">
	<h1 class="sink_dashicon dashicons-share"><?php echo get_bloginfo('title') ?> Options</h1>
	<h4>Social Media Integration by <a href="http://social-ink.net">Social Ink</a></h4>	
	<?php if($media_options) {	?>
		<form method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">
			<?php	foreach($media_options as $media) {
				$media_slug = 'socialink_site' . $media;?>
					<div class="theme_section">
						<h3><?php echo ucfirst($media) ?> Link</h3>		
						<input type="text" style="width:80%" name="<?php echo $media_slug ?>"  id="<?php echo $media_slug ?>" value="<?php echo get_option($media_slug) ?>">
					</div>
				<?php } ?>			
				<div class="theme_section">
					<h3>Icon Prefix</h3>		
					<input type="text" style="width:80%" name="sink_client_mediaoption_prefix"  id="sink_client_mediaoption_prefix" value="<?php echo get_option('sink_client_mediaoption_prefix') ?>">
				</div>
				
			<p>
				<input type="hidden" name="submit_form_fields" value="Y">
				<input type="submit" class="button-primary" value="Update All Changes" />	
			</p>		
		</form>
		<?php } ?>		
</div>