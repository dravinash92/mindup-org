<?php
if($_POST['submit_form_fields'] == 'Y') {
	foreach($_POST as $key => $value) {
		if($key!="[submit_form_fields]" && ($value!=""))
			set_sinktheme_option($key, $value);
	}	
	echo '<div class="updated"><p><strong>Theme options have been updated.</strong></p></div>';
}
global $wpeditor_settings;
global $options_array;
?>
<div class="wrap socialink_theme_options">
	<h1 class="sink_dashicon dashicons-admin-plugins"><?php echo get_bloginfo('title') ?> Options</h1>
	<h4>Custom theme options by <a href="http://social-ink.net">Social Ink</a></h4>
	<?php if($options_array && (SINK_CLIENT_OPTIONPAGE === TRUE)) {	?>
		<form method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">
			<?php	foreach($options_array as $ftoption) {	//dump($ftoption);?>
					<div class="theme_section">
					<h3><?php echo $ftoption['caption'] ?></h3>
					<?php if(isset($ftoption['tip'])) { ?>
						<p><?php echo $ftoption['tip'] ?></p>
					<?php } ?>						
					<?php if(isset($ftoption['type']) && $ftoption['type']=='text') { ?>
						<input type="text" style="width:80%" name="<?php echo $ftoption['name'] ?>"  id="<?php echo $ftoption['name'] ?>" value="<?php echo get_option($ftoption['name']) ?>">
					<?php } elseif(!isset($ftoption['post_type'])) {
						$ftoption_slug = $ftoption['name'];
						$$ftoption_slug = convert_to_html(get_option($ftoption_slug));	?>
							<?php wp_editor($$ftoption_slug,$ftoption_slug, $wpeditor_settings); ?>				
					<?php } elseif(isset($ftoption['post_type'])) {
							$args = array(
							'post_type' => 		$ftoption['post_type'],
							'showposts'	=>		-1,
							'orderby'	=>		'name'
								);		
							$myposts = get_posts( $args );
							if($myposts) { 
						?>
							<select name="<?php echo $ftoption['name'] ?>" id="<?php echo $ftoption['name'] ?>">					
							<?php	foreach($myposts as $post) {?>			
									<option <?php if (get_option($ftoption['name'])==$post->ID) echo 'selected="selected"' ?> value="<?php echo $post->ID ?>"><?php echo $post->post_title ?></option>
								<?php } ?>
							</select>
						<?php } ?>
				<?php } ?>			
				</div>
			<?php } ?>		
			<p>
				<input type="hidden" name="submit_form_fields" value="Y">
				<input type="submit" class="button-primary" value="Update All Changes" />	
			</p>		
		</form>
	<?php } else { ?>
		<p>There don't seem to be any options today!  If you think something is missing, please visit us at <a href="http://social-ink.net">http://social-ink.net</a> and get in touch!</p>
		<p>Thanks, the gang at Social Ink</p>
	<?php } ?>
</div>