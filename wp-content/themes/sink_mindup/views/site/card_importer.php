<!-- card import : BEGIN -->
<?php
	// get card contents
	if (get_field('card_grouping')) {
		$card_group = get_field('card_grouping');
		$card_groups = count(get_field('card_grouping'));
		
		// card group loop : BEGIN	
		foreach ($card_group as $key => $g) {

			// echo '<pre>';
			// var_dump($g);
			// echo '</pre>';

			$group_header 	= $g['card_group_title'];
			$group_header_style = $g['card_group_title_style'];
			$group_text			= $g['card_group_text'];
			$group_display 	= $g['card_display'];
			$group_quant		= $g['quantity_up'];
			$card_type 			= $g['card_type_chooser'];
			// lazy class explodes
			if ($card_type == 'header-cards-cta') {
				$card_type = 'header-cards cta';
			} elseif ($card_type == 'file-cards-cta') {
				$card_type = 'file-cards cta';
			} elseif ($card_type == 'logo-cards-two-up') {
				$card_type = 'logo-cards two-up';
			}

			$card_counter		= count($g['card_group_types']);
			$cc		= $g['card_group_types']; ?>
					<!-- group section : BEGIN -->
						<div class="<?php echo $group_display; ?>">
							<section class="container-md px-md-0">
								<?php if ( ( isset($group_header) ) || ( ( isset($group_text) ) ) ) : ?>
									<div class="row">
										<div class="col-12 <?php echo ($group_header_style == 'gradient-head') ? 'mx-0' : 'mx-3' ?>">
										<?php if ( isset($group_header) ) : ?>
											<h2 class="<?php echo $group_header_style; ?>" role="header"><?php echo $group_header; ?></h2>
										<?php endif; ?>
										<?php if ( isset($group_text) ) : ?>
											<p><?php echo $group_text; ?></p>
										<?php endif; ?>
										</div>
									</div>
								<?php endif; ?>
								<div class="row new-founder-columns row-cols-1 <?php echo $card_type . ' ' .$group_quant; ?> h-100">
						
										<?php		

											// individual card loop	: BEGIN
												foreach( $cc as $card_key => $card ) { ?>
													<?php // get card link type from card_link / card_internal_link / card_special_link
														
														if ($card['card_internal_link']) {
																$card_link = $card['card_internal_link']['url'];
																$card_title = $card['card_internal_link']['title'];
																$card_target = $card['card_internal_link']['target'] ? $card['card_internal_link']['target'] : '_self';
														}
														 elseif ( isset($card['card_special_link']) ) {
															$card_link = $card['card_special_link'];
															$card_title = $card['card_special_link'];
															$card_target = '_self';
														} else {
															$card_link = '#';
															$card_title = 'none';
															$card_target = '_self';
														}

													?>
													<?php
														if(($card_type == 'video-cards') || ($card_type == 'expert-cards')) {
															$center_flex = 'justify-content-center d-flex d-md-block';
														} else {
															$center_flex = '';
														}
													?>
													<div id="<?php echo $card_type.'-'.$card_key; ?>" class="col card-col <?php echo $center_flex; ?>">
														<div class="card h-100">
															<?php 
															// 1 logo cards
																if ($card_type == 'logo-cards') : ?>
																<img src="<?php echo $card['img_upload_1']['url'] ?>" alt="<?php echo $card['img_upload_1']['alt'] ?>" class="card-img-top" />
															<?php 
															// 2 logo cards
																elseif ($card_type == 'logo-cards two-up' ) : ?>
																	<div class="two-logos">
																		<img src="<?php echo $card['img_upload_1']['url'] ?>" alt="<?php echo $card['img_upload_1']['alt'] ?>" class="card-img-top" />
																		<img src="<?php echo $card['img_upload_2']['url'] ?>" alt="<?php echo $card['img_upload_2']['alt'] ?>" class="card-img-top" />
																	</div>

															

															<?php
															// File / Expert 
																elseif (($card_type == 'file-cards') || ($card_type == 'expert-cards') ) : ?>
																<div style="background-image: url(<?php echo $card['img_upload_1']['url'] ?>);" class="card-img-top" aria-label="<?php echo $card['img_upload_1']['alt'] ?>"></div>

															<?php // Video Cards
																elseif ($card_type == 'video-cards') : ?>
																<a href="#" title="<?php echo $card_title; ?>" data-toggle="modal" data-target="#videoModal" data-url="<?php echo $card_link; ?>">
																	<div style="background-image: url(<?php echo $card['img_upload_1']['url'] ?>);" class="card-img-top" aria-label="<?php echo $card['img_upload_1']['alt'] ?>"></div>
																</a>
															
															<?php endif; ?>

															<?php if (($card['card_header'] != '') || ($card['card_copy']) != '') : ?>
																<div class="card-body">
																	<?php if (isset($card['card_header']) && (($card_type == 'header-cards') || ($card_type == 'header-cards cta') || ($card_type == 'expert-cards'))) : ?>
																		<p class="card-text header"><?php echo $card['card_header'] ?></p>
																	<?php endif; ?>
																	<?php if ((isset($card['card_copy']) && ($card_type != 'expert-cards'))) : ?>
																		<p class="card-text"><?php echo $card['card_copy'] ?></p>
																	<?php endif; ?>
																	<?php if ( (isset($card['card_copy']) && ($card_type == 'expert-cards'))) : ?>
																		<p class="card-text expert"><span>Expert:</span><br/><?php echo $card['card_copy'] ?></p>
																	<?php endif; ?>
																</div>
															<?php endif; ?>

															<div class="card-footer">

																


																<?php if($card_type != 'video-cards') : ?>
																	<!-- All other Card link -->
																	<p class="text-center"><a href="<?php echo $card_link; ?>" target="<?php echo $card_target; ?>" class="btn btn-primary <?php echo $card['card_button_type']; ?>" title="<?php echo $card_title; ?>"><?php echo $card['card_button_text']; ?></a></p>
																<?php else : ?>
																	<!-- Video Card link -->
																	<p class="<?php echo $card['card_button_type']; ?>"><a href="#" title="<?php echo $card_title; ?>" data-toggle="modal" data-target="#videoModal" data-url="<?php echo $card_link; ?>"><?php echo $card['card_button_text']; ?></a></p>
																<?php endif; ?>
															</div>

														</div>
													</div>
												<?php }
											// individual card loop	: END  ?>					
								</div>
							</section>
						</div>
					<!-- group section : END -->

		<?php } // card group loop : END

		} // end card contents
	?>
<!-- card import : END -->