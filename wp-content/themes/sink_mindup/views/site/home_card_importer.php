<!-- card import : BEGIN -->
<?php
	// get card contents
	if (get_field('home_card_grouping')) {
		$home_card_group = get_field('home_card_grouping');
		$home_card_groups = count(get_field('home_card_grouping'));
		
		// card group loop : BEGIN	
		foreach ($home_card_group as $key => $h) {

			// echo '<pre>';
			// var_dump($h);
			// echo '</pre>';

			
			$home_group_quant		= $h['home_quantity_up'];
			$home_card_type 			= $h['home_card_type_chooser'];
			// lazy class explodes
			if ($home_card_type == 'header-cards-cta') {
				$home_card_type = 'header-cards cta';
			} elseif ($home_card_type == 'file-cards-cta') {
				$home_card_type = 'file-cards cta';
			} elseif ($home_card_type == 'logo-cards-two-up') {
				$home_card_type = 'logo-cards two-up';
			}

			$home_card_counter		= count($h['home_card_group_types']);
			$hc		= $h['home_card_group_types']; ?>
					<!-- group section : BEGIN -->
						<div class="row new-founder-columns row-cols-1 <?php echo $home_card_type . ' ' .$home_group_quant; ?> h-100">
				
								<?php		

									// individual card loop	: BEGIN
										foreach( $hc as $home_card_key => $home_card ) { ?>
											<?php
												if(($home_card_type == 'video-cards') || ($home_card_type == 'expert-cards')) {
													$center_flex = 'justify-content-center d-flex d-md-block';
												} else {
													$center_flex = '';
												}
											?>
											<div id="<?php echo $home_card_type.'-'.$home_card_key; ?>" class="col card-col <?php echo $center_flex; ?>">
												<div class="card h-100">
													<?php 
													// 1 logo cards
														if ($home_card_type == 'logo-cards') : ?>
														<img src="<?php echo $home_card['home_image_upload_1']['url'] ?>" alt="<?php echo $home_card['home_image_upload_1']['alt'] ?>" class="card-img-top" />
													<?php 
													// 2 logo cards
														elseif ($home_card_type == 'logo-cards two-up' ) : ?>
															<div class="two-logos">
																<img src="<?php echo $home_card['home_image_upload_1']['url'] ?>" alt="<?php echo $home_card['home_image_upload_1']['alt'] ?>" class="card-img-top" />
																<img src="<?php echo $home_card['home_image_upload_2']['url'] ?>" alt="<?php echo $home_card['home_image_upload_2']['alt'] ?>" class="card-img-top" />
															</div>

													<?php
													// File / Expert / Video Cards
														elseif (($home_card_type == 'file-cards') || ($home_card_type == 'expert-cards') || ($home_card_type == 'video-cards') ) : ?>
														<div style="background-image: url(<?php echo $home_card['home_image_upload_1']['url'] ?>);" class="card-img-top" aria-label="<?php echo $home_card['home_image_upload_1']['alt'] ?>"></div>
													
													<?php endif; ?>

													<?php if (($home_card['home_card_header'] != '') || ($home_card['home_card_copy']) != '') : ?>
														<div class="card-body">
															<?php if (isset($home_card['home_card_header']) && (($home_card_type == 'header-cards') || ($home_card_type == 'header-cards cta') || ($home_card_type == 'expert-cards'))) : ?>
																<p class="card-text header"><?php echo $home_card['home_card_header'] ?></p>
															<?php endif; ?>
															<?php if ((isset($home_card['home_card_copy']) && ($home_card_type != 'expert-cards'))) : ?>
																<p class="card-text"><?php echo $home_card['home_card_copy'] ?></p>
															<?php endif; ?>
															<?php if ( (isset($home_card['home_card_copy']) && ($home_card_type == 'expert-cards'))) : ?>
																<p class="card-text expert"><span>Expert:</span><br/><?php echo $home_card['home_card_copy'] ?></p>
															<?php endif; ?>
														</div>
													<?php endif; ?>

													<div class="card-footer">

														<?php // get card link type from card_link / card_internal_link / card_special_link
															
															if ($home_card['home_card_link']) {
																	$home_card_link = $home_card['home_card_link']['url'];
																	$home_card_title = $home_card['home_card_link']['title'];
																	$home_card_target = $home_card['home_card_link']['target'] ? $home_card['home_card_link']['target'] : '_self';

																	// echo $home_card_link.'<br />';
																	// echo $home_card_title.'<br />';
																	// echo $home_card_target.'<br />';
															}
															 elseif ( isset($home_card['home_card_special_link']) ) {
																$home_card_link = $home_card['home_card_special_link'];
																$home_card_title = $home_card['home_card_special_link'];
																$home_card_target = '_self';
																// echo $home_card_link.'<br />';
																// echo $home_card_title.'<br />';
																// echo $home_card_target.'<br />';
															} else {
																$home_card_link = '#';
																$home_card_title = 'none';
																$home_card_target = '_self';
															}

														?>


														<?php if($home_card_type != 'video-cards') : ?>
															<!-- All other Card link -->
															<p class="text-center"><a href="<?php echo $home_card_link; ?>" target="<?php echo $home_card_target; ?>" class="btn btn-primary <?php echo $home_card['home_card_button_type']; ?>" title="<?php echo $home_card_title; ?>"><?php echo $home_card['home_card_button_text']; ?></a></p>
														<?php else : ?>
															<!-- Video Card link -->
															<p class="<?php echo $home_card['home_card_button_type']; ?>"><a href="#" title="<?php echo $home_card_title; ?>" data-toggle="modal" data-target="#videoModal" data-url="<?php echo $home_card_link; ?>" ><?php echo $home_card['home_card_button_text']; ?></a></p>
														<?php endif; ?>
													</div>

												</div>
											</div>
										<?php }
									// individual card loop	: END  ?>					
						</div>
					<!-- group section : END -->

		<?php } // card group loop : END

		} // end card contents
	?>
<!-- card import : END -->