<?php
$media_options = explode(',',SINK_CLIENT_MEDIAOPTIONS);
$prefix = get_option('sink_client_mediaoption_prefix') ? get_option('sink_client_mediaoption_prefix') . '-' : '';
?>
<?php if($media_options) { ?>
<nav class="socialmedia menu_horizontal" role="social_media">
	<ul>
	<?php foreach($media_options as $media) {
		$media_slug = 'socialink_site' . $media;
		if($link = get_option($media_slug)) { ?>
			<li class="media-link media-link-<?php echo $media ?>"><a class="<?php echo $media ?> socialmedia_clicker" title="See <?php echo SINK_CLIENT ?> on <?php echo ucfirst($media) ?>" href="<?php echo $link ?>"><?php echo get_sink_icon($media) ?></a></li>
		<?php	}
	}	?>
	</ul>
</nav>
<?php } ?>
