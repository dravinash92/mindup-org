<!-- pillar card import : BEGIN -->
<?php
	// get card contents
	if (get_field('pillar_card_grouping')) {
		$pillar_group = get_field('pillar_card_grouping');
		$pillar_groups = count(get_field('pillar_card_grouping'));
		
		// card group loop : BEGIN	
		foreach ($pillar_group as $key => $p) {

			// echo '<pre>';
			// var_dump($p);
			// echo '</pre>';

			$pillar_header 	= $p['pillar_card_group_title'];
			$pillar_header_style = $p['pillar_card_group_title_style'];
			$pillar_text			= $p['pillar_card_group_text'];
			$pillar_display 	= $p['pillar_card_display'];
			$pillar_quant		= $p['pillar_quantity_up'];
			$pillar_type 			= $p['pillar_card_type_chooser'];
			// lazy class explodes
			if ($pillar_type == 'header-cards-cta') {
				$pillar_type = 'header-cards cta';
			} elseif ($pillar_type == 'file-cards-cta') {
				$pillar_type = 'file-cards cta';
			} elseif ($pillar_type == 'logo-cards-two-up') {
				$pillar_type = 'logo-cards two-up';
			}

			$pillar_counter		= count($p['pillar_card_group_types']);
			$pc		= $p['pillar_card_group_types']; ?>
					<!-- group section : BEGIN -->
						<div class="<?php echo $pillar_display; ?>">
							<section class="container-md px-md-0">
								<?php if ( ( isset($pillar_header) ) || ( ( isset($pillar_text) ) ) ) : ?>
									<div class="row">
										<div class="col-12 <?php echo ($pillar_header_style == 'gradient-head') ? 'mx-0' : 'mx-3' ?>">
										<?php if ( isset($pillar_header) ) : ?>
											<h2 class="<?php echo $pillar_header_style; ?>" role="header"><?php echo $pillar_header; ?></h2>
										<?php endif; ?>
										<?php if ( isset($pillar_text) ) : ?>
											<p><?php echo $pillar_text; ?></p>
										<?php endif; ?>
										</div>
									</div>
								<?php endif; ?>
								<div class="row four-pillar-columns row-cols-1 <?php echo $pillar_type . ' ' .$pillar_quant; ?> h-100">
						
										<?php		

											// individual card loop	: BEGIN
												foreach( $pc as $pillar_key => $pillar ) { ?>
													<?php
														if($pillar_type == 'pillar-cards') {
															$center_flex = 'justify-content-center d-flex d-md-block';
														} else {
															$center_flex = '';
														}
													?>
													<div id="<?php echo $pillar_type.'-'.$pillar_key; ?>" class="col card-col <?php echo $center_flex; ?>">

														<div class="card h-100">
															
															<?php 
															// pillar header with img flex : BEGIN
																if ($pillar_type == 'pillar-cards' ) : ?>
																	<div class="pillar-header align-items-center">
																		<img src="<?php echo $pillar['pillar_img_upload_1']['url'] ?>" alt="<?php echo $pillar['pillar_img_upload_1']['alt']?>" class="card-img-top" width="88px" height="88px" />

																		<?php if (isset($pillar['pillar_card_header'])) : ?>
																			<p class="header-text"><?php echo $pillar['pillar_card_header'] ?></p>
																		<?php endif; ?>
																	</div>													
															
															<?php endif;
															// pillar header with img flex : END
															?>
															<div class="card-body">
																
																<?php if (isset($pillar['pillar_card_copy'])) : ?>
																	<p class="card-text"><?php echo $pillar['pillar_card_copy'] ?></p>
																<?php endif; ?>
																
															</div>

															

														</div>
													</div>
												<?php }
											// individual card loop	: END  ?>					
								</div>
							</section>
						</div>
					<!-- group section : END -->

		<?php } // card group loop : END

		} // card contents : END
	?>
<!-- pillar card import : END -->