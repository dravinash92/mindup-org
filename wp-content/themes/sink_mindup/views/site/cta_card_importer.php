<!-- cta card import : BEGIN -->
<?php
	// get card contents
	if (get_field('cta_card_grouping')) {
		$cta_card_group = get_field('cta_card_grouping');
		$cta_card_groups = count(get_field('cta_card_grouping'));
		
		// card group loop : BEGIN	
		foreach ($cta_card_group as $key => $c) {

			// echo '<pre>';
			// var_dump($c);
			// echo '</pre>';


			$cta_card_type 			= $c['cta_card_type_chooser'];
			// lazy class explodes
			if ($cta_card_type == 'header-cards-cta') {
				$cta_card_type = 'header-cards cta';
			} elseif ($cta_card_type == 'file-cards-cta') {
				$cta_card_type = 'file-cards cta';
			} elseif ($cta_card_type == 'logo-cards-two-up') {
				$cta_card_type = 'logo-cards two-up';
			}

			$cta_card_counter		= count($c['cta_card_group_types']);
			$cta		= $c['cta_card_group_types']; ?>
					<!-- group section : BEGIN -->								
								<div class="row new-founder-columns row-cols-1 <?php echo $cta_card_type ; ?> ">
						
										<?php		
											// individual card loop	: BEGIN
												foreach( $cta as $cta_card_key => $card_cta ) { ?>
													<?php
														if(($cta_card_type == 'video-cards') || ($cta_card_type == 'expert-cards')) {
															$center_flex = 'justify-content-center d-flex d-md-block';
														} else {
															$center_flex = '';
														}
													?>
													<div id="<?php echo $cta_card_type.'-'.$cta_card_key; ?>" class="col card-col <?php echo $center_flex; ?>">
														<div class="card h-100">
															<?php 
															// 1 logo cards
																if ($cta_card_type == 'logo-cards') : ?>
																<img src="<?php echo $card_cta['cta_image_upload_1']['url'] ?>" alt="<?php echo $card_cta['cta_image_upload_1']['alt'] ?>" class="card-img-top" />
															<?php 
															// 2 logo cards
																elseif ($cta_card_type == 'logo-cards two-up' ) : ?>
																	<div class="two-logos">
																		<img src="<?php echo $card_cta['cta_image_upload_1']['url'] ?>" alt="<?php echo $card_cta['cta_image_upload_1']['alt'] ?>" class="card-img-top" />
																		<img src="<?php echo $card_cta['cta_image_upload_2']['url'] ?>" alt="<?php echo $card_cta['cta_image_upload_2']['alt'] ?>" class="card-img-top" />
																	</div>

															<?php
															// File / Expert / Video Cards
																elseif (($cta_card_type == 'file-cards') || ($cta_card_type == 'file-cards cta') || ($cta_card_type == 'expert-cards') || ($cta_card_type == 'video-cards') ) : ?>
																<div style="background-image: url(<?php echo $card_cta['cta_image_upload_1']['url'] ?>);" class="card-img-top" aria-label="<?php echo $card_cta['cta_image_upload_1']['alt'] ?>"></div>
															
															<?php endif; ?>

															<?php if (($card_cta['cta_card_header'] != '') || ($card_cta['cta_card_copy']) != '') : ?>
																<div class="card-body">
																	<?php if (isset($card_cta['cta_card_header']) && (($cta_card_type == 'header-cards') || ($cta_card_type == 'header-cards cta') || ($cta_card_type == 'expert-cards'))) : ?>
																		<p class="card-text header"><?php echo $card_cta['cta_card_header'] ?></p>
																	<?php endif; ?>
																	<?php if ((isset($card_cta['cta_card_copy']) && ($cta_card_type != 'expert-cards'))) : ?>
																		<p class="card-text"><?php echo $card_cta['cta_card_copy'] ?></p>
																	<?php endif; ?>
																	<?php if ( (isset($card_cta['cta_card_copy']) && ($cta_card_type == 'expert-cards'))) : ?>
																		<p class="card-text expert"><span>Expert:</span><br/><?php echo $card_cta['cta_card_copy'] ?></p>
																	<?php endif; ?>
																</div>
															<?php endif; ?>

															<div class="card-footer">

																<?php // get card link type from card_link / card_internal_link / card_special_link
																	
																	if ($card_cta['cta_card_internal_link']) {
																			$cta_card_link = $card_cta['cta_card_internal_link']['url'];
																			$cta_card_title = $card_cta['cta_card_internal_link']['title'];
																			$cta_card_target = $card_cta['cta_card_internal_link']['target'] ? $card_cta['cta_card_internal_link']['target'] : '_self';

																			// echo $cta_card_link.'<br />';
																			// echo $cta_card_title.'<br />';
																			// echo $cta_card_target.'<br />';
																	}
																	 elseif ( isset($card_cta['cta_card_special_link']) ) {
																		$cta_card_link = $card_cta['cta_card_special_link'];
																		$cta_card_title = 'none';
																		$cta_card_target = '_self';
																		// echo $cta_card_link.'<br />';
																		// echo $cta_card_title.'<br />';
																		// echo $cta_card_target.'<br />';
																	} else {
																		$cta_card_link = '#';
																		$cta_card_title = 'none';
																		$cta_card_target = '_self';
																	}

																?>


																<?php if($cta_card_type != 'video-cards') : ?>
																	<!-- All other Card link -->
																	<p class="text-center"><a href="<?php echo $cta_card_link; ?>" target="<?php echo $cta_card_target; ?>" class="btn btn-primary <?php echo $card_cta['cta_card_button_type']; ?>" title="<?php echo $cta_card_title; ?>"><?php echo $card_cta['cta_card_button_text']; ?></a></p>
																<?php else : ?>
																	<!-- Video Card link -->
																	<p class="<?php echo $card_cta['cta_card_button_type']; ?>"><a href="<?php echo $cta_card_link; ?>" target="<?php echo $cta_card_target; ?>" title="<?php echo $cta_card_title; ?>"><?php echo $card_cta['cta_card_button_text']; ?></a></p>
																<?php endif; ?>
															</div>

														</div>
													</div>
												<?php }
											// individual card loop	: END  ?>					
								</div>
							
					<!-- group section : END -->

		<?php } // card group loop : END

		} // end card contents
	?>
<!-- cta card import : END -->