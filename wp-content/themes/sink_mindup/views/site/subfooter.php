<!-- site/subfooter.php : BEGIN -->
<footer class="subfooter" style="background: linear-gradient(135deg, #D600B3 0%, #004BFF 100%) !important;color: #ffff !important;width: 100% !important;height: 250px !important;padding: 0px !important;">
	<div class="carousel-container">
		<?php  if($testimonials = get_testimonials_carousel()) {
			$show_class = (count($testimonials) == 1) ? 'noslides' : 'enabled';
			?>
			<div class="slideshow_area <?php  echo $show_class ?>">
				<?php  foreach($testimonials as $slide) {
					?>
					<div class="slide">
						<div class="slideinner">
							<div class="body"><?php echo $slide->post_content ?></div>
							<?php echo printmeta('name' ,'p', $slide) ?>
							<?php echo printmeta('subtitle__role' ,'p', $slide) ?>
						</div>
					</div>
				<?php  } ?>
			</div>
			<?php  if(count($testimonials) > 1) { ?>
				<div class="slide_nav left"><a title="Previous slide" href="#"><?php echo get_sink_icon('arrow-left') ?></a></div>
				<div class="slide_nav right"><a title="Next slide" href=""><?php echo get_sink_icon('arrow-right') ?></a></div>
			<?php  } ?>
		<?php  } ?>
	</div>
</footer>
<div class="clearfix"></div>
<!-- site/subfooter.php : END -->