<!-- site/header-inner.php : BEGIN -->
	<!-- mobile only : BEGIN -->
		<div class="mobile_only">
			<?php	echo do_action('socialink_mindup_header') ?>
		</div>

		<div class="header_inner mobile_only">
			<nav class="menu_nav" role="navigation">
				<div class="centering_box burger-wrap">
					<div class="burger-time">
						<input type="checkbox" id="btnControl"/>
						<div class="add-heat">
							<label class="hamburger-btn" for="btnControl">
								<div class="burger burger-container">
									<span class="burger-bu invisible-burger"></span>
									<span class="burger-meat"></span>
									<span class="burger-bun"></span>
									<span class="burger-bun"></span>
								</div>
							</label>
							<span class="text_assist">MENU</span>
							<?php	echo get_nav_easy() ?>
						</div>
					</div>
				</div>
			</nav>
			<div class="logo">
				<a class="logoclick dont_highlight"  href="<?php echo get_option('home'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/logo.png" title="<?php echo get_bloginfo('title') ?> - <?php echo get_bloginfo('description') ?>" alt="<?php echo get_bloginfo('title') ?> - <?php echo get_bloginfo('description') ?>" /></a>
			</div>
		</div>
	<!-- mobile only : END -->
	<!-- desktop only : BEGIN -->
		<div class="header_inner desktop_only flexible menu_horizontal">
			<div class="logo">
				<a class="logoclick dont_highlight"  href="<?php echo get_option('home'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/logo.png" title="<?php echo get_bloginfo('title') ?> - <?php echo get_bloginfo('description') ?>" alt="<?php echo get_bloginfo('title') ?> - <?php echo get_bloginfo('description') ?>" /></a>
			</div>
			<nav class="topmenu"><?php	echo get_nav_easy() ?></nav>
			<?php	echo do_action('socialink_mindup_header') ?>
		</div>
	<!-- desktop only : BEGIN -->
<!-- site/header-inner.php : END -->
