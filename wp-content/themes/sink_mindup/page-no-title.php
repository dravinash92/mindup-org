<?php
/*
Template Name: Page - No Title
*/
?>
<?php
	get_header();
	global $post;
	$main = $post;
	the_post();
 ?>
<!-- page-no-title.php : BEGIN -->
<main class="onepage" id="maincontent" role="main">
<section class="content centering_box">
	<article <?php post_class('copy') ?>>
		<div class="text">
			<?php the_content(); ?>
		</div>
	</article>
</section>
<?php echo socialink_pagebuilder() ?>
</main>
<?php get_footer(); ?>