<?php
add_filter( 'password_protected_login_password_title', 'socialink_change_login_label' );
function socialink_change_login_label($label) {
    $label = "Please enter your password to enter the website";
    return $label;
}
function socialink_add_login_css() {
    wp_enqueue_style( 'socialink-custom-login', get_bloginfo('template_url') . '/assets/css/login.css');
}
add_action( 'login_enqueue_scripts', 'socialink_add_login_css' );
add_filter( 'plugins_auto_update_enabled', '__return_false' );
add_filter( 'themes_auto_update_enabled', '__return_false' );
function remove_site_health_menu() {
    remove_submenu_page( 'tools.php', 'site-health.php' );
}
add_action( 'admin_menu', 'remove_site_health_menu' );
add_action('wp_dashboard_setup', 'remove_site_health_dashboard_widget');
function remove_site_health_dashboard_widget()
{
    remove_meta_box('dashboard_site_health', 'dashboard', 'normal');
}
add_filter('body_class', 'header_add_logged_in_user');
function header_add_logged_in_user($classes) {
    if(is_user_logged_in()) {
        global $current_user;
        $userClass = 'loggedin-user-' . $current_user->ID;
        $classes[] = $userClass;
    }
     return $classes;
}
add_filter( 'admin_body_class', 'socialink_admin_body_class' );
function socialink_admin_body_class( $classes ) {
    global $post;
        $classes .= ' socialink-pid-' . $post->ID;

    return $classes;
}
add_action( 'admin_bar_menu', 'remove_some_nodes_from_admin_top_bar_menu', 999 );
function remove_some_nodes_from_admin_top_bar_menu( $wp_admin_bar ) {
    $wp_admin_bar->remove_menu( 'customize' );
    $wp_admin_bar->remove_menu('comments');
}
add_action( 'admin_menu', function () {
    global $submenu;
    if ( isset( $submenu[ 'themes.php' ] ) ) {
        foreach ( $submenu[ 'themes.php' ] as $index => $menu_item ) {
            if ( in_array(  'customize', $menu_item ) ) {
                unset( $submenu[ 'themes.php' ][ $index ] );
            }
        }
    }
});
function socialink_enqueue_custom_admin_style() {
        wp_register_style( 'socialink_wp_admin_css', get_template_directory_uri() . '/assets/css/admin.css', false, '1.0.0' );
        wp_enqueue_style( 'socialink_wp_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'socialink_enqueue_custom_admin_style' );
function move_yoast_below_acf() {
    return 'low';
}
add_filter( 'wpseo_metabox_prio', 'move_yoast_below_acf');
add_filter( "use_block_editor_for_post_type", "__return_false", 100 );
function theme_slug_setup() {
   add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'theme_slug_setup' );
add_filter( 'ac/suppress_site_wide_notices', '__return_true' );
apply_filters( 'auto_core_update_send_email', true);
register_nav_menus( array(
    'main_menu' => 'Main Navigation',
    'footer_menu' => 'Footer Navigation'
) );
$img_def = array(
    'medium'    =>  array(640,99999),
    'large' =>  array(960,99999)
    );
set_post_thumbnail_size( 200, 200 );
foreach($img_def as $def => $dimensions) {
    update_option($def . '_size_w', $dimensions[0]);
    update_option($def . '_size_h', $dimensions[1]);
}
function ses_tinymce_css($wp) {
$wp .= ',' . get_bloginfo('template_url') . '/assets/css/tinymce.css';
return $wp;
}
add_filter( 'mce_css', 'ses_tinymce_css' );
add_action( 'admin_footer-post-new.php', 'wpse_76048_script' );
add_action( 'admin_footer-post.php', 'wpse_76048_script' );
function sink_iconify_menu($atts, $item, $args ) {
    $prefix = (SINK_CLIENT_SHORT!="") ? strtolower(SINK_CLIENT_SHORT) : strtolower(sanitize_title(get_bloginfo('title')));
    $atts['class'] = $prefix. '-' . sanitize_title($item->title) . '-' . $item->db_id;
    return $atts;
}
    add_filter('nav_menu_link_attributes', 'sink_iconify_menu',10,6);
function wpse_76048_script()
{
    ?>
<script>
jQuery(function($) {
    var called = 0;
    $('#wpcontent').ajaxStop(function() {
        if ( 0 == called ) {
            $('[value="uploaded"]').attr( 'selected', true ).parent().trigger('change');
            called = 1;
        }
    });
});
</script>
    <?php
}
add_action('acf/input/admin_head', 'socialink_acf_admin_head');
function socialink_acf_admin_head() {
    ?>
    <script type="text/javascript">
    (function($) {
        $(document).ready(function(){
            var $msg_fld = $(".acf-field[data-key='<? echo SINK_CLIENT_CONTENTFLD ?>']").append( $('#postdivrich') );
            var $msg_fld = $(".acf-field[data-key='field_5fca7a3298391']").append( $('#postdivrich') );
            var $msg_fld = $(".acf-field[data-key='<? echo SINK_CLIENT_EXCERPTFLD ?>']").append( $('#postexcerpt') );
            var $msg_fld = $(".acf-field[data-key='field_5fca7a32983cc']").append( $('#postexcerpt') );
        });

    })(jQuery);
    </script>
    <style type="text/css">
        .acf-field #wp-content-editor-tools {
            background: transparent;
            padding-top: 0;
        }
        .acf-field .postbox {
            background: transparent none repeat scroll 0 0;
            border: 0 none;
            box-shadow: none;
            min-width: inherit;
            padding-top: 0;
        }

            .acf-field .postbox h2,
            .acf-field .postbox p {
                display:none;
            }
            .acf-field #postexcerpt {
                display:block !important;
            }
    </style>
    <?php
}
if ( ! function_exists( 'twentyeleven_comment' ) ) :
function twentyeleven_comment( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;
    switch ( $comment->comment_type ) :
        case 'pingback' :
        case 'trackback' :
    ?>
    <li class="post pingback">
        <p><?php _e( 'Pingback:', 'twentyeleven' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?></p>
    <?php
            break;
        default :
    ?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
        <article id="comment-<?php comment_ID(); ?>" class="comment">
            <footer class="comment-meta">
                <div class="comment-author vcard">
                    <?php
                        $avatar_size = 68;
                        if ( '0' != $comment->comment_parent )
                            $avatar_size = 39;
                        echo get_avatar( $comment, $avatar_size );
                        /* translators: 1: comment author, 2: date and time */
                        printf( __( '%1$s on %2$s <span class="says">said:</span>', 'twentyeleven' ),
                            sprintf( '<span class="fn">%s</span>', get_comment_author_link() ),
                            sprintf( '<a href="%1$s"><time pubdate datetime="%2$s">%3$s</time></a>',
                                esc_url( get_comment_link( $comment->comment_ID ) ),
                                get_comment_time( 'c' ),
                                /* translators: 1: date, 2: time */
                                sprintf( __( '%1$s at %2$s', 'twentyeleven' ), get_comment_date(), get_comment_time() )
                            )
                        );
                    ?>
                    <?php edit_comment_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?>
                </div><!-- .comment-author .vcard -->
                <?php if ( $comment->comment_approved == '0' ) : ?>
                    <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'twentyeleven' ); ?></em>
                    <br />
                <?php endif; ?>
            </footer>
            <div class="comment-content"><?php comment_text(); ?></div>
            <div class="reply">
                <?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply <span>&darr;</span>', 'twentyeleven' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
            </div><!-- .reply -->
        </article><!-- #comment-## -->
    <?php
            break;
    endswitch;
}
endif; // ends check for twentyeleven_comment()
add_action('wp_head','sinktheme_ajaxify');
    function sinktheme_ajaxify() {
    ?>
    <script type="text/javascript">
        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
    </script>
    <?php
}
function socialink_login_logo() { ?>
    <style type="text/css">
        body {
            background:#fff !important;
        }
        .login h1 {
            width:inherit !important;
        }
        .login form {
            box-shadow:inherit !important;
        }
        .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png) !important;
            background-size: 100% auto !important;
            padding-bottom: 30px !important;
            width: 200px !important;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'socialink_login_logo' );
function socialink_login_url() {
    return home_url();
}
add_action('admin_head', 'socialink_core_stuff');
function socialink_core_stuff() {
    $style = "<style>
                // #adminmenu div.wp-menu-image.svg {
                // background-repeat: no-repeat;
                // background-position: -2px 0px;
                // -webkit-background-size: 50px auto;
                // background-size: 50px auto;
            </style>";
    echo $style;
}
add_filter( 'login_headerurl', 'socialink_login_url' );
function socialink_login_url_title() {
    return get_bloginfo('title');
}
add_filter( 'login_headertitle', 'socialink_login_url_title' );
function socialink_remove_menus(){

  // remove_menu_page( 'index.php' );                  //Dashboard
  // remove_menu_page( 'jetpack' );                    //Jetpack*
  // remove_menu_page( 'edit.php' );                   //Posts
  // remove_menu_page( 'upload.php' );                 //Media
  // remove_menu_page( 'edit.php?post_type=page' );    //Pages
  remove_menu_page( 'edit-comments.php' );          //Comments
  // remove_menu_page( 'themes.php' );                 //Appearance
  // remove_menu_page( 'plugins.php' );                //Plugins
  // remove_menu_page( 'users.php' );                  //Users
  // remove_menu_page( 'tools.php' );                  //Tools
  // remove_menu_page( 'options-general.php' );        //Settings

}
add_action( 'admin_menu', 'socialink_remove_menus' );
?>