<?php

class sinkMemberPress {

	var $group_signup_id = 221;

	function getRequestQuotePage() {
		return get_permalink($this->group_signup_id);
	}

	function getSchoolRequestPriceBox() {
		include(get_template_directory() . '/views/memberpress/pricebox-custom-school.php');
	}

}