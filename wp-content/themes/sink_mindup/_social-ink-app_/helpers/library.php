<?php
/* 	COPYRIGHT SOCIAL INK (C) 2020
THIS CODE LIBRARY AND FUNCTIONS/SUBROUTINES HEREIN ARE PROPERTY OF SOCIAL INK,
AND MAY NOT BE REPRODUCED, MODIFIED OR SOLD, EXCEPT AS STIPULATED IN CONTRACT WITH SOCIAL INK.
NO OTHER DEVELOPERS MAY USE ANY PART OF THE CODE BELOW FOR ANY OTHER PROJECT, OR REUSE THIS CODE
IN REDEVELOPMENT OF THIS CLIENT'S SITE WITHOUT PERMISSION OR LICENSING FROM SOCIAL INK.
INFO@SOCIAL-INK.NET
*/
//MENUS
function get_nav_easy($menu = 'main' , $type = 'sink_dropdowns') {
	$mymenu = wp_get_nav_menu_object($menu);
	if($mymenu) {
		if($type == 'sink_dropdowns' && function_exists('get_sink_dropdown')) {
			$mymenu = get_sink_dropdown($menu);
		} else {
			$mymenu = wp_nav_menu(array(
				'menu' => $menu,
				'container' => 'false',
				'echo' => false,
				'walker' => new Aria_Walker_Nav_Menu(),
				'items_wrap'     => '<ul id="%1$s" class="%2$s" role="menubar">%3$s</ul>',
			)
		);
		}
	}
	return $mymenu;
}
//CONTENT
function get_testimonials_carousel() {
	$testimonials = false;
	$args = array(
		'post_type'	=>	'testimonial',
		'showposts'	=>	-1,
		'orderby'	=>	'rand',
	);
	if($posts = get_posts($args)) {
		foreach($posts as $post)
			$testimonials[$post->ID] = parse_item($post);
	}
	return $testimonials;
}
function parse_item($item) {
	$func = "parse_$item->post_type";

	if(function_exists($func))
		return call_user_func($func,$item);
	else {
		$item->mainlabel = $item->post_title;
		$item->sublabel = false;
		return $item;
	}
}
function mk_button($lbl, $url, $cls = 'green', $icon = '') {
	if(!$lbl || !$url)
		return false;
	ob_start();
	?>
	<a href="<?php echo $url ?>" class="button button_<?php echo $cls ?>"><?php echo get_sink_icon($icon) ?><?php echo $lbl ?></a>
	<?php
	return ob_get_clean();
}
function exportOptionsHTML($string) {
	return apply_filters('the_content', $string);
}
function get_sinktheme_option($option, $format = true) {
	if(get_option($option))
		return $format ? apply_filters('the_content', get_option($option)) : get_option($option);
}
function set_sinktheme_option($option, $string) {
	$string = str_replace('\"', "", $string);
	$string = str_replace("\'", "'", $string);
	return update_option($option, urldecode($string));
}
function get_slug($post_id=false) {
	if(!$post_id) {
		global $post;
		$post_id=$post->ID;
	}
	$post_data = get_post($post_id, ARRAY_A);
	$slug = $post_data['post_name'];
	return $slug;
}
function get_meu_excerpt($post_id=false, $trim=40, $backoff=false) {
	global $post;
	$temp_post = $post;
	if(!$post_id) {
		global $post;
		$post_id=$post->ID;
	}
	$post = get_post($post_id);
	setup_postdata($post);
	ob_start();
	echo get_the_excerpt();
	$post_excerpt = ob_get_clean();
	$post = $temp_post;
	return $trim ? trim_excerpt($post_excerpt,$trim,$backoff) : $post_excerpt;
}
function trim_excerpt($str,$trim=40, $backoff='...') {
	$words = explode(' ', $str);
	$revised = array_slice($words,0,$trim);
	$str = implode(' ',$revised) . $backoff;
	return $str;
}
function is_post_type_single() {
	global $post;
	$single_type = false;
	$allCPTs = get_post_types();
	foreach($allCPTs as $oneCPT) {
		$metainfo = get_post_type_object($oneCPT);
		if($post->post_type==$metainfo->name)
			$single_type = true;
	}
	return $single_type;
}
function convert_to_html($string) {
	$string = str_replace('\"', "", $string);
	return $string;
}
function gen_slug($str){
	return sanitize_title($str);
}
function snk_content_filter($content) {
	return $content;
}
//IMAGES
function get_hero_image_for($item,$cf = 'hero_image', $sz = 'hero') {
	return get_hero_image($cf, 'hero', true, $item);
}
function format_hero_img($img_src) {
	return 'style="background-image:url(' . $img_src . ')"';
}
function get_hero_caption($cf = 'hero_image', $sz = 'hero') {
	if($hero = get_hero_image($cf, $sz, false)) {
		return $hero['caption'];
	}
	else
		return false;
}
function get_hero_image($cf = 'hero_image', $sz = 'full', $styled = true, $item_override = false) {
	$hero_obj = new stdclass();
	if($item_override)
		$item = $item_override;
	elseif(is_single() || is_singular()) {
		global $post;
		$item = $post;
	} elseif(!is_post_type_archive()) {
		$item = get_queried_object();
	}

	if($this_object = gf($cf, $item)) {
		$hero = $this_object;
	} elseif($global = gf('default_hero','option')) {
		$hero = array($global);
	}

	if($hero && (count($hero) == 1)) {
		$hero = reset($hero);
		$hero_obj->exists = true;
		$hero_obj->type = "image";
		$hero_obj->caption = ($hero['caption'] != "") ? $hero['caption'] : false;
		$hero_obj->size = 1;
		$hero_obj->img = ($sz != 'full') ? $hero['sizes'][$sz] : $hero['url'];
		$hero_obj->bgcode = 'style="background-image:url(\'' . $hero_obj->img . '\')"';
		return $hero_obj;
	} elseif($hero && isset($hero['ID'])) {
		$hero_obj->exists = true;
		$hero_obj->type = "image";
		$hero_obj->size = 1;
		$hero_obj->img = ($sz != 'full') ? $hero['sizes'][$sz] : $hero['url'];
		$hero_obj->bgcode = 'style="background-image:url(\'' . $hero_obj->img . '\')"';
		return $hero_obj;
	} elseif($hero) { //slideshow
		$hero_obj->exists = true;
		$hero_obj->type = "slideshow";
		$hero_obj->size = count($hero);
		$hero_obj->slides= json_decode(json_encode($hero),false);
		$hero_obj->mainslide = reset($hero_obj->slides);
		$hero_obj->mainslide_img = ($sz != 'full') ? $hero_obj->mainslide->sizes->$sz : $hero_obj->mainslide->url;
		$hero_obj->bgcode = 'style="background-image:url(\'' . $hero_obj->mainslide_img . '\')"';
		return $hero_obj;
	}
	else
		return false;
}
function hero_override_default() {
	$hero = false;
	$sinkLearnDash = new sinksinkLearnDash();
	if($sinkLearnDash->ld_is_in_course_mode()) {
	     $hero = get_hero_image_for('option','default_course_header_image');
	} else {
	     $hero = get_hero_image('hero_image');
	}
	//$hero = get_hero_image('hero_image');
	return $hero;
}
function get_hero_class($cf = 'hero_image', $sz = 'full') {
	$hero_class = 'hero-none';
	$hero = hero_override_default();
	if($hero) {
		if($hero->type =='image')
			$hero_class = 'hero-exists hero-image';
		elseif($hero->type =='slideshow')
			$hero_class = 'hero-exists hero-slideshow';
	}
	return $hero_class;
}
function get_option_img($option, $sz = 'hero', $styled = true) {
	$hero = get_field($option,'option');
	if($hero && $styled) {
		$img = ($sz != 'full') ? $hero['sizes'][$sz] : $hero['url'];
		return 'style="background-image:url(\'' . $img . '\')"';
	}
	elseif($hero)
		return $hero;
	else
		return false;
}
function gf($cf,$item = false) {
	if(!$item) {
		global $post;
		$item = $post;
	}
	$item_id = is_object($item) ? $item->ID : $item;
	if(function_exists('get_field'))
		return get_field($cf, $item);
	else
		return get_post_meta($item_id,$cf,true);
}
function parse_archive_meta() {
	$archive = new stdclass();
	$title = "";
	$slug = "";
	global $wp_query;
	$obj = get_queried_object();
	if (is_post_type_archive()) {
		$title = $wp_query->queried_object->labels->name;
		$archive_slug = $wp_query->queried_object;
		$status_label = 'All Resources';
		echo 'a';
	} elseif($obj instanceof WP_Term) {
		$title = $obj->name;
		$slug = $obj->slug;
		if(is_tax('resources')) {
			$status_label = 'Resources in ' . $title;
			$title = 'Resources: ' . $title;
		}
	} elseif (is_category()) {
		$title = single_cat_title();
	} elseif( is_tag() ) {
		$title = single_tag_title();
	} elseif (is_day()) {
		$title = the_time('F jS, Y');
	} elseif (is_month()) {
		$title = the_time('F, Y');
	} elseif (is_year()) {
		$title = the_time('Y');
	} elseif (is_author()) {
		$title = 'Content by Author <span class="author_name">' . $obj->data->display_name . '</span>';
	} elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {
		$title = 'Blog Archives';
	}
	$archive->title = $title;
	$archive->slug = $slug;
	$archive->description = property_exists($obj,'description') ? $obj->description : false;
	return $archive;
}
?>