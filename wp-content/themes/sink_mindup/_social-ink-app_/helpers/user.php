<?php
add_action('socialink_mindup_header', 'userfunctions_header');
add_action('socialink_mindup_footer', 'userfunctions_footer');
add_filter('body_class', 'userfunctions_bodyclasses');
function userfunctions_header() {
	include(get_template_directory() . '/views/user/header.php');
}
function userfunctions_footer() {
	include(get_template_directory() . '/views/user/footer.php');
}
function userfunctions_bodyclasses($classes) {
	$sinkMindUpUser = new sinkMindUpUser();
	if($user = $sinkMindUpUser->getUser()) {
		$userClass = 'memberpress-user-' . $user->ID;
		$classes[] = $userClass;
	}
	return $classes;
}
class sinkMindUpUser {
	var $user_logged_in = false;
	var $user = false;
	var $mPModel = false;
	var $signup_page_id = 90;
	var $course_dashboard_id = 111;
	var $login_page_id = 61;
	function __construct() {
		$this->user_logged_in = is_user_logged_in();
		$this->user = wp_get_current_user();
		$this->setUserMP();
	}
	function setUserMP() {
		if($user = $this->getUser()) {
			$this->mPModel = new MeprUser($user->ID);
			if($this->mPModel->is_active()) {
				$this->user->memberPressSubs = $this->mPModel->active_product_subscriptions();
			}
		}
	}
	function getUserSignupPage() {
		return get_permalink($this->signup_page_id);
	}
	function getUserCourseDashboard() {
		return get_permalink($this->course_dashboard_id);
	}
	function getUserLogIn() {
		return get_permalink($this->login_page_id);
	}
	function getUserMenu() {
		return get_nav_easy('user-menu', false);
	}
	function getUser() {
		return ($this->user && $this->user_logged_in) ? $this->user : false;
	}
	function userfunctions_footer_classes() {
		$class = "footer-logged-out";
		if($this->getUser()) {
			$class = "footer-logged-in";
		}
		return $class;
	}
	function userfunctions_header_classes() {
		$class = "header-logged-out";
		if($this->getUser()) {
			$class = "header-logged-in";
		}
		return $class;
	}
}