<?php
/*
Template Name: Homepage HTML Templates
*/
?>
<?php 	get_header();
		global $post;
		$main = $post;
		the_post();
 ?>
<!-- page-template-homepage.php | TEMPLATE: Homepage : BEGIN -->
<main class="onepage" id="maincontent" role="main">
<section class="content centering_box">
	<article <?php post_class('copy') ?>>
		<div class="text">
			<?php the_content(); ?>
		</div>
	</article>
</section>
<section class="join-section">
	<div class="centering_box">
		<h1>Join Our Amazing Community</h1>
		<p class="description">Membership Options</p>
		<div class="group-plans-embed">
			<? echo do_shortcode('[mepr-group-price-boxes group_id="90"]') ?>
		</div>

		<p class="description"><a href="home/plans/membership-options/" class="gradient-default button">Join Our Online Community</a></p>
        <blockquote class="description">MindUP reinforces the enjoyment and impact of the experience of learning.<br>&mdash; Daniel J. Siegel, MD</blockquote>

	</div>
</section>
<? if($cols = get_field('founders_columns')) { ?>
<section class="founders-columns">
	<div class="flexible">
		<? foreach($cols as $i => $col) { ?>
			<div class="founder_column col-12 col-md-6 <? //echo $i ?>">
				<? if($col['column_header']) { ?>
					<h1><? echo $col['column_header'] ?></h1>
				<? } ?>
				<? if($col['column_contents']) { ?>
					<div class=" column_contents"><? echo $col['column_contents'] ?></div>
				<? } ?>
			</div>
	 	<? } ?>
	</div>
</section>
<? } ?>

	<!-- logo cards : BEGIN -->
		<div class="no-back">
			<section class="container-md px-md-0">
				<div class="row new-founder-columns logo-cards row-cols-1 row-cols-md-3 h-100">
					<div class="col card-col">
							<div class="card h-100">
								<img src="<?php echo get_theme_file_uri() ?>/images/logocard-mindup-for-schools.png" class="card-img-top" />
								
								<div class="card-footer">
									<p class="text-center"><a href="#" class="btn btn-primary btn-grad-1">Learn More</a></p>
								</div>
							</div>
					</div>
					<div class="col card-col">
							<div class="card h-100">
								<img src="<?php echo get_theme_file_uri() ?>/images/logocard-mindup-for-families.png" class="card-img-top" />
								
								<div class="card-footer">
									<p class="text-center"><a href="#" class="btn btn-primary btn-grad-1">Learn More</a></p>
								</div>
							</div>
					</div>
					<div class="col card-col">
						<div class="card h-100">
							<img src="<?php echo get_theme_file_uri() ?>/images/logocard-mindup-for-adults.png" class="card-img-top" />
							
							<div class="card-footer">
								<p class="text-center"><a href="#" class="btn btn-primary btn-grad-1">Learn More</a></p>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	<!-- logo cards : END -->

	<!-- logo copy cards : BEGIN -->
		<div class="no-back">
			<section class="container-md px-md-0">
				<div class="row new-founder-columns logo-cards logo-copy row-cols-1 row-cols-md-3 h-100">
					<div class="col card-col">
							<div class="card h-100">
								<img src="<?php echo get_theme_file_uri() ?>/images/logocard-mindup-for-schools.png" class="card-img-top" />
								<div class="card-body">
									<p class="card-text">Explore MindUP for Schools courses, resources and activities.</p>
								</div>
								<div class="card-footer">
									<p class="text-center"><a href="#" class="btn btn-primary btn-grad-1">Learn More</a></p>
								</div>
							</div>
					</div>
					<div class="col card-col">
							<div class="card h-100">
								<img src="<?php echo get_theme_file_uri() ?>/images/logocard-mindup-for-families.png" class="card-img-top" />
								<div class="card-body">
									<p class="card-text">Lorem ipsum dolor text text text text. Explore MindUP for Families courses, resources and activities.</p>
								</div>
								<div class="card-footer">
									<p class="text-center"><a href="#" class="btn btn-primary btn-grad-1">Learn More</a></p>
								</div>
							</div>
					</div>
					<div class="col card-col">
						<div class="card h-100">
							<img src="<?php echo get_theme_file_uri() ?>/images/logocard-mindup-for-adults.png" class="card-img-top" />
							<div class="card-body">
								<p class="card-text">Explore MindUP for Adults courses, resources and activities.</p>
							</div>
							<div class="card-footer">
								<p class="text-center"><a href="#" class="btn btn-primary btn-grad-1">Learn More</a></p>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	<!-- logo copy cards : END -->

	<!-- 2 logo cards : BEGIN -->
		<div class="no-back">
			<section class="container-md px-md-0">
				<div class="row new-founder-columns logo-cards two-up row-cols-1 row-cols-md-3 h-100">
					<div class="col card-col">
							<div class="card h-100">
								<div class="two-logos">
									<img src="<?php echo get_theme_file_uri() ?>/images/logocard-mindup-for-life.png" class="card-img-top" />
									<img src="<?php echo get_theme_file_uri() ?>/images/logocard-moshi.png" class="card-img-top" />
								</div>
								<div class="card-body">
									<p class="card-text">Explore MindUP for Schools courses, resources and activities.</p>
								</div>
								<div class="card-footer">
									<p class="text-center"><a href="#" class="btn btn-primary btn-grad-1">Learn More</a></p>
								</div>
							</div>
					</div>
					<div class="col card-col">
							<div class="card h-100">
								<div class="two-logos">
									<img src="<?php echo get_theme_file_uri() ?>/images/logocard-mindup-for-life.png" class="card-img-top" />
									<img src="<?php echo get_theme_file_uri() ?>/images/logocard-insighttimer.png" class="card-img-top" />
								</div>

								<div class="card-body">
									<p class="card-text">Lorem ipsum dolor text text text text. Explore MindUP for Families courses, resources and activities.</p>
								</div>
								<div class="card-footer">
									<p class="text-center"><a href="#" class="btn btn-primary btn-grad-1">Learn More</a></p>
								</div>
							</div>
					</div>
					<div class="col card-col">
						<div class="card h-100">
							<div class="two-logos">
								<img src="<?php echo get_theme_file_uri() ?>/images/logocard-mindup-for-life.png" class="card-img-top" />
								<img src="<?php echo get_theme_file_uri() ?>/images/logocard-ocean-heroes.png" class="card-img-top" />
							</div>
							<div class="card-body">
								<p class="card-text">Explore MindUP for Adults courses, resources and activities.</p>
							</div>
							<div class="card-footer">
								<p class="text-center"><a href="#" class="btn btn-primary btn-grad-1">Learn More</a></p>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	<!-- 2 logo cards : END -->

	<!-- header cards : BEGIN -->
		<div class="no-back">
			<section class="container-md px-md-0">
				<div class="row">
					<div class="col-12 mx-0">
						<h2 class="gradient-head" role="header">Training &amp; Online Memberships</h2>
					</div>
				</div>
				<div class="row new-founder-columns header-cards row-cols-1 row-cols-md-2 h-100">
					<div class="col card-col">
						<div class="card h-100">
							
							<div class="card-body">
								<p class="card-text header">Header</p>
								
							</div>
							<div class="card-footer">
								<p class="text-center"><a href="#" class="btn btn-primary btn-grad-1">Learn More</a></p>
							</div>
						</div>
					</div>

					<div class="col card-col">
						<div class="card h-100">
							
							<div class="card-body">
								<p class="card-text header">Header</p>
								
							</div>
							<div class="card-footer">
								<p class="text-center"><a href="#" class="btn btn-primary btn-grad-1">Learn More</a></p>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	<!-- header cards : END -->

	<!-- header cards CTA : BEGIN -->
		<div class="color-back">
			<section class="container-md px-md-0">
				<div class="row new-founder-columns cta header-cards row-cols-1 row-cols-md-2 h-100">
					<div class="col card-col">
						<div class="card h-100">
							
							<div class="card-body">
								<p class="card-text header">Header</p>
								
							</div>
							<div class="card-footer">
								<p class="text-center"><a href="#" class="btn btn-primary btn-white">Learn More</a></p>
							</div>
						</div>
					</div>

					<div class="col card-col">
						<div class="card h-100">
							
							<div class="card-body">
								<p class="card-text header">Header</p>
								
							</div>
							<div class="card-footer">
								<p class="text-center"><a href="#" class="btn btn-primary btn-white">Learn More</a></p>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	<!-- header cards CTA : END -->

	<!-- file cards : BEGIN -->
		<div class="gradient-back-magenta">
			<section class="container-md px-md-0">
				<div class="row new-founder-columns file-cards row-cols-1 row-cols-md-3 h-100">
					<div class="col card-col">
						<div class="card h-100">
							<div style="background-image: url(<?php echo get_theme_file_uri() ?>/images/poster-acts-of-kindness.jpg);" class="card-img-top" ></div>
							<div class="card-body">
								<p class="card-text">File Name/CTA</p>
							</div>
							<div class="card-footer">
								<p class="text-center"><a href="#" class="btn btn-primary btn-grad-1">Download</a></p>
							</div>
						</div>
					</div>
					<div class="col card-col">
						<div class="card h-100">
							<div style="background-image: url(<?php echo get_theme_file_uri() ?>/images/poster-acts-of-kindness.jpg);" class="card-img-top" ></div>
							<div class="card-body">
								<p class="card-text">File Name/CTA</p>
							</div>
							<div class="card-footer">
								<p class="text-center"><a href="#" class="btn btn-primary btn-grad-1">Download</a></p>
							</div>
						</div>
					</div>
					<div class="col card-col">
						<div class="card h-100">
							<div style="background-image: url(<?php echo get_theme_file_uri() ?>/images/poster-acts-of-kindness.jpg);" class="card-img-top" ></div>
							<div class="card-body">
								<p class="card-text">File Name/CTA</p>
							</div>
							<div class="card-footer">
								<p class="text-center"><a href="#" class="btn btn-primary btn-grad-1">Download</a></p>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	<!-- file cards : END -->

	<!-- file cards CTA : BEGIN -->
		<div class="color-back">
			<section class="container-md px-md-0">
				<div class="row new-founder-columns cta file-cards row-cols-1 row-cols-md-2 h-100">
					<div class="col card-col">
						<div class="card h-100">
							<div style="background-image: url(<?php echo get_theme_file_uri() ?>/images/poster-acts-of-kindness.jpg);" class="card-img-top" ></div>
							<div class="card-body">
								<p class="card-text">File Name/CTA</p>
							</div>
							<div class="card-footer">
								<p class="text-center"><a href="#" class="btn btn-primary btn-white">Download</a></p>
							</div>
						</div>
					</div>
					<div class="col card-col">
						<div class="card h-100">
							<div style="background-image: url(<?php echo get_theme_file_uri() ?>/images/poster-acts-of-kindness.jpg);" class="card-img-top" ></div>
							<div class="card-body">
								<p class="card-text">File Name/CTA</p>
							</div>
							<div class="card-footer">
								<p class="text-center"><a href="#" class="btn btn-primary btn-white">Download</a></p>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	<!-- file cards CTA : END -->

	<!-- video cards : BEGIN -->
		<div class="no-back">
			<section class="container-md px-md-0">
					<div class="row new-founder-columns video-cards row-cols-1 row-cols-md-3 h-100">
						<div class="col card-col justify-content-center d-flex">
							<div class="card h-100">
								<div style="background-image: url(<?php echo get_theme_file_uri() ?>/images/goldie-hawn-barb-schmidt.jpg);" class="card-img-top" ></div>
								<div class="card-body">
									<p class="card-text">Mindfulness with Goldie Hawn and Barb Schmidt</p>
								</div>
								<div class="card-footer">
									<p class=""><a href="#" class="video-cta">Watch the Video</a></p>
								</div>
							</div>
						</div>
						<div class="col card-col justify-content-center d-flex">
							<div class="card h-100">
								<div style="background-image: url(<?php echo get_theme_file_uri() ?>/images/goldie-hawn-barb-schmidt.jpg);" class="card-img-top" ></div>
								<div class="card-body">
									<p class="card-text">Mindfulness with Goldie Hawn and Barb Schmidt</p>
								</div>
								<div class="card-footer">
									<p class=""><a href="#" class="video-cta">Watch the Video</a></p>
								</div>
							</div>
						</div>
						<div class="col card-col justify-content-center d-flex">
							<div class="card h-100">
								<div style="background-image: url(<?php echo get_theme_file_uri() ?>/images/goldie-hawn-barb-schmidt.jpg);" class="card-img-top" ></div>
								<div class="card-body">
									<p class="card-text">Mindfulness with Goldie Hawn and Barb Schmidt</p>
								</div>
								<div class="card-footer">
									<p class=""><a href="#" class="video-cta">Watch the Video</a></p>
								</div>
							</div>
						</div>
					</div>
			</section>
		</div>
	<!-- video cards : END -->

	<!-- Expert cards : BEGIN -->
		<div class="no-back">
			<section class="container-md px-md-0">
				<div class="row new-founder-columns expert-cards row-cols-1 row-cols-md-3 h-100">
					<div class="col card-col justify-content-center d-flex">
						<div class="card h-100">
							<div style="background-image: url(<?php echo get_theme_file_uri() ?>/images/expert-kimberly-schonertreichl.jpg);" class="card-img-top" ></div>
							<div class="card-body">
								<p class="card-text header">Kimberly Schonert-Reichl, Ph.D</p>
								<p class="card-text">Expert:</p>
								<p class="card-text expert">Social and Emotional Learning</p>
							</div>
							<div class="card-footer">
								<p class="text-center"><a href="#" class="btn btn-primary btn-grad-1">See Videos</a></p>
							</div>
						</div>
					</div>
					<div class="col card-col justify-content-center d-flex">
						<div class="card h-100">
							<div style="background-image: url(<?php echo get_theme_file_uri() ?>/images/expert-kimberly-schonertreichl.jpg);" class="card-img-top" ></div>
							<div class="card-body">
								<p class="card-text name">Kimberly Schonert-Reichl, Ph.D</p>
								<p class="card-text">Expert:</p>
								<p class="card-text expert">Social and Emotional Learning</p>
							</div>
							<div class="card-footer">
								<p class="text-center"><a href="#" class="btn btn-primary btn-grad-1">See Videos</a></p>
							</div>
						</div>
					</div>
					<div class="col card-col justify-content-center d-flex">
						<div class="card h-100">
							<div style="background-image: url(<?php echo get_theme_file_uri() ?>/images/expert-kimberly-schonertreichl.jpg);" class="card-img-top" ></div>
							<div class="card-body">
								<p class="card-text name">Kimbserly Schonert-Reichl, Ph.D</p>
								<p class="card-text">Expert:</p>
								<p class="card-text expert">Social and Emotional Learning</p>
							</div>
							<div class="card-footer">
								<p class="text-center"><a href="#" class="btn btn-primary btn-grad-1">See Videos</a></p>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	<!-- Expert cards : END -->

	<!-- four-pillar-columns : BEGIN -->
		<div class="no-back">
			<section class="container-md px-md-0">
				<div class="row four-pillar-columns row-cols-1 row-cols-md-2 h-100">
					<div class="col card-col pillar-cards justify-content-center d-flex d-md-block">
						<div class="card h-100">
							<div class="pillar-header align-items-center">

								<img src="<?php echo get_theme_file_uri() ?>/images/mindfulawareness-magenta-88x88.svg" class="card-img-top" />
								<p class="header-text">Positive Psychology</p>
							</div>
							<div class="card-body">
								<p class="card-text">Positive psychology is the study of the strengths that contribute to individual and community thriving and well-being. MindUP infuses evidence-based strategies from the field of positive psychology to help students thrive, and bolster their well-being.</p>
							</div>
						</div>
					</div>
					<div class="col card-col pillar-cards  justify-content-center d-flex d-md-block">
						<div class="card h-100">
							<div class="pillar-header align-items-center">

								<img src="<?php echo get_theme_file_uri() ?>/images/mindfulawareness-magenta-88x88.svg" class="card-img-top" />
								<p class="header-text">Positive Psychology</p>
							</div>
							<div class="card-body">
								<p class="card-text">Positive psychology is the study of the strengths that contribute to individual and community thriving and well-being. MindUP infuses evidence-based strategies from the field of positive psychology to help students thrive, and bolster their well-being.</p>
							</div>
						</div>
					</div>
					<div class="col card-col pillar-cards  justify-content-center d-flex d-md-block">
						<div class="card h-100">
							<div class="pillar-header align-items-center">

								<img src="<?php echo get_theme_file_uri() ?>/images/mindfulawareness-magenta-88x88.svg" class="card-img-top" />
								<p class="header-text ">Positive Psychology</p>
							</div>
							<div class="card-body">
								<p class="card-text">Positive psychology is the study of the strengths that contribute to individual and community thriving and well-being. MindUP infuses evidence-based strategies from the field of positive psychology to help students thrive, and bolster their well-being.</p>
							</div>
						</div>
					</div>
					<div class="col card-col pillar-cards justify-content-center d-flex d-md-block">
						<div class="card h-100">
							<div class="pillar-header align-items-center">

								<img src="<?php echo get_theme_file_uri() ?>/images/mindfulawareness-magenta-88x88.svg" class="card-img-top" />
								<p class="header-text">Positive Psychology</p>
							</div>
							<div class="card-body">
								<p class="card-text">Positive psychology is the study of the strengths that contribute to individual and community thriving and well-being. MindUP infuses evidence-based strategies from the field of positive psychology to help students thrive, and bolster their well-being.</p>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	<!-- four-pillar-columns : END -->

</main>
<!-- page-template-homepage.php | TEMPLATE: Homepage : END -->
<?php get_footer(); ?>
